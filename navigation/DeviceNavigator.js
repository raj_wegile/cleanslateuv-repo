import { createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import DeviceDetailScreen from '../screens/DeviceUser/DeviceDetailsScreen'
import DeviceConfirmScreen from '../screens/DeviceUser/DeviceConfirmScreen'
import {Image} from 'react-native'
import React from 'react';
const logo = require('../images/uvlogo1.png');

const DeviceNavigator = createStackNavigator(
  {
      DeviceInfo: {
        screen: DeviceConfirmScreen,
        navigationOptions: {
          title: ' ',
          headerLeft: () => null,
          headerTitle: () => <Image source={logo}
            style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
          headerStyle: {
            backgroundColor: "white"
          },
          headerTitleStyle: {
            fontWeight: "bold",
          },
          headerTitleAlign: 'center',
        },
      },
      DeviceDetail:
      {
        screen: DeviceDetailScreen,
        navigationOptions: {
          title: ' ',
          headerTitle: () => <Image source={logo}
            style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
          headerStyle: {
            backgroundColor: "white"
          },
          headerTitleStyle: {
            fontWeight: "bold",
          },
          headerTitleAlign: 'center',
        },
      },
  },
)

const getStateForActionScreensStack = DeviceNavigator.router.getStateForAction;

DeviceNavigator.router = {
    ...DeviceNavigator.router,
    getStateForAction(action, state) {
    
      if (action.type == 'Navigation/BACK' && state.index == 0) {
        const routes = [
          {routeName: 'DeviceInfo'},
        ];
        return {
          ...state,
          routes,
          index: 0,
        };
      }
      return getStateForActionScreensStack(action, state);
    },
};
export default DeviceNavigator;

