import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import DeviceAuthScreen from '../screens/DeviceUser/DeviceAuthScreen';
import ManagerAuthScreen from '../screens/ManagerAuthScreen';
import LoginOptions from '../screens/LoginOptions';
import SetupScreen from '../screens/SetupScreen';
import DeviceNavigator from './DeviceNavigator';
import AuthConfirmScreen from '../screens/DeviceUser/AuthConfirmScreen';
import ForgotPasswordNavigator from './ForgotPasswordNavigator';
import BluetoothSetupScreen from '../screens/Setup/BluetoothSetupScreen';
import WifiSetupScreen from '../screens/Setup/WifiSetupScreen';
import FirmwareUpdateScreen from '../screens/Setup/FirmwareUpdateScreen';
import BluetoothDownloadScreen from '../screens/Setup/BluetoothDownloadScreen';
import DepartManagerNavigator from './DepartManagerNavigator';
import HospitalManagerNavigator from './HospitalManagerNavigator';
import {Image} from 'react-native'
import React from 'react';
const logo = require('../images/uvlogo1.png');

const ManagerAuthNavigator = createStackNavigator(
  {
    ManagerAuth: {
      screen: ManagerAuthScreen,
      navigationOptions: {
        title: ' ',
        headerLeft: () => null,
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
        gestureEnabled: false
      },
    },
    ForgotPassword: {
      screen: ForgotPasswordNavigator,
      navigationOptions: {
        title: ' ',
        headerShown: false
      },
    },
    DepartManager: {
      screen: DepartManagerNavigator,
      navigationOptions: {
        title: ' ',
        headerShown: false,
        gestureEnabled: false
      },
    },
    HospitalManager: {
      screen: HospitalManagerNavigator,
      navigationOptions: {
        title: ' ',
        headerShown: false,
        gestureEnabled: false
      },
    },
  },
)

const DeviceAuthNavigator = createStackNavigator(
  {
    DeviceAuth: {
      screen: DeviceAuthScreen,
      navigationOptions: {
        title: ' ',
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
      },
    },
    AuthConfirm: {
      screen: AuthConfirmScreen,
      navigationOptions: {
        title: ' ',
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
      },
    },
    DeviceMain: {
      screen: DeviceNavigator,
      navigationOptions: {
        title: ' ',
        headerShown: false,
        gestureEnabled: false
      },
    },
  }
)

const SetupNavigator = createStackNavigator(
  {
    SetupHome: {
      screen: SetupScreen,
      navigationOptions: {
        title: ' ',
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
      },
    },
    BluetoothSetup: {
      screen: BluetoothSetupScreen,
      navigationOptions: {
        title: ' ',
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
      },
    },
    WifiSetup: {
      screen: WifiSetupScreen,
      navigationOptions: {
        title: ' ',
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
      },
    },
    FirmwareUpdate: {
      screen: FirmwareUpdateScreen,
      navigationOptions: {
        title: ' ',
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
      },
    },
    BluetoothDownload: {
      screen: BluetoothDownloadScreen,
      navigationOptions: {
        title: ' ',
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
      },
    },
  },
)

// The main container for navigating through the app screens
const MainNavigator = createStackNavigator(
  {
    // This screen is used to navigate to either the login screen
    // Or the automatic sign-in if credentials have been previously entered
    ManagerLoginOptions: {
      screen: LoginOptions,
      navigationOptions: {
        title: ' ',
        headerLeft: () => null,
        headerTitle: () => <Image source={logo}
          style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
        headerStyle: {
          backgroundColor: "white"
        },
        headerTitleStyle: {
          fontWeight: "bold",
        },
        headerTitleAlign: 'center',
        gestureEnabled: false
      },
    },
    // This page is not currently being used
    // Directs the user to the device (RFID tagged) screens
    DeviceMain: {
      screen: DeviceAuthNavigator,
      navigationOptions: {
        title: ' ',
        headerShown: false,
        gestureEnabled: false
      }
    },
    // Users navigate here when not already authenticated
    // Handles user sign-in and password forgetting
    ManagerMain: {
      screen: ManagerAuthNavigator,
      navigationOptions: {
        title: ' ',
        headerShown: false,
        gestureEnabled: false
      },
    },
    // Handles the setup screens that are current under development
    SetupMain: {
      screen: SetupNavigator,
      navigationOptions: {
        title: ' ',
        headerShown: false,
      }
    }
  },
)

export default createAppContainer(MainNavigator);