
import { createStackNavigator } from 'react-navigation-stack';
import HospitalManagerScreen from '../screens/HospiManager/HospitalManagerScreen';
import HMCleanslateScreen from '../screens/HospiManager/HMCleanslateScreen';
import CleanslateScreen from '../screens/HospiManager/CleanslateScreen'
import {Image} from 'react-native'
import React from 'react';
const logo = require('../images/uvlogo1.png')


const HospitalManagerNavigator = createStackNavigator(
    {
        HospitalManagerHome: {
          screen: HospitalManagerScreen,
          navigationOptions: {
            title: ' ',
            headerLeft: () => null,
            headerTitle: () => <Image source={logo}
              style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
            headerStyle: {
              backgroundColor: "white"
            },
            headerTitleStyle: {
              fontWeight: "bold",
            },
            headerTitleAlign: 'center',
          },
        },
        HMCleanslateScreen: {
          screen: HMCleanslateScreen,
          navigationOptions: {
            title: ' ',
            headerTitle: () => <Image source={logo}
              style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
            headerStyle: {
              backgroundColor: "white"
            },
            headerTitleStyle: {
              fontWeight: "bold",
            },
            headerTitleAlign: 'center',
          },
        },
        CleanslateScreen:{
          screen:CleanslateScreen,
          navigationOptions: {
            title: ' ',
            headerTitle: () => <Image source={logo}
              style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
            headerStyle: {
              backgroundColor: "white"
            },
            headerTitleStyle: {
              fontWeight: "bold",
            },
            headerTitleAlign: 'center',
          },
        },
      },
  )

const getStateForActionScreensStack = HospitalManagerNavigator.router.getStateForAction;

HospitalManagerNavigator.router = {
    ...HospitalManagerNavigator.router,
    getStateForAction(action, state) {
    
      if (action.type == 'Navigation/BACK' && state.index == 0) {
        console.log('We are going back...');
        console.log(state);
        console.log(action);
        const routes = [
          {routeName: 'HospitalManagerHome'},
        ];
        return {
          ...state,
          routes,
          index: 0,
        };
      }
      return getStateForActionScreensStack(action, state);
    },
};

export default HospitalManagerNavigator