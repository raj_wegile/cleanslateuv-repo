import { createStackNavigator } from 'react-navigation-stack';
import DepartmentManagerScreen from '../screens/DepartManager/DepartmentManagerScreen';
import DMCleanslateScreen from '../screens/DepartManager/DMCleanslateScreen';
import {Image} from 'react-native'
import React from 'react';
const logo = require('../images/uvlogo1.png')

const DepartManagerNavigator = createStackNavigator(
    {
        DepartManagerHome: {
          screen: DepartmentManagerScreen,
          navigationOptions: {
            title: ' ',
            headerLeft: () => null,
            headerTitle: () => <Image source={logo}
              style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
            headerStyle: {
              backgroundColor: "white"
            },
            headerTitleStyle: {
              fontWeight: "bold",
            },
            headerTitleAlign: 'center',
          },
        },
        DMCleanslateScreen: {
          screen: DMCleanslateScreen,
          navigationOptions: {
            title: ' ',
            headerTitle: () => <Image source={logo}
              style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
            headerStyle: {
              backgroundColor: "white"
            },
            headerTitleStyle: {
              fontWeight: "bold",
            },
            headerTitleAlign: 'center',
          },
        },
    },
  )
  
export default DepartManagerNavigator;