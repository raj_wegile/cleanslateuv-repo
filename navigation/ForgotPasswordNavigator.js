import { createStackNavigator } from 'react-navigation-stack';
import ForgotPasswordScreen from '../screens/ForgotPassword';
import PasswordConfirmationScreen from '../screens/PasswordConfirmation';
import {Image} from 'react-native'
import React from 'react';
const logo = require('../images/uvlogo1.png')

const ForgotPasswordNavigator = createStackNavigator(
    {
        ForgotPassword: {
          screen: ForgotPasswordScreen,
          navigationOptions: {
            title: ' ',
            headerTitle: () => <Image source={logo}
              style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
            headerStyle: {
              backgroundColor: "white"
            },
            headerTitleStyle: {
              fontWeight: "bold",
            },
            headerTitleAlign: 'center',
          },
        },
        PasswordConfirmation: {
          screen: PasswordConfirmationScreen,
          navigationOptions: {
            title: ' ',
            headerTitle: () => <Image source={logo}
              style={{ width: 200, height: 200, resizeMode: 'contain' }}/>,
            headerStyle: {
              backgroundColor: "white"
            },
            headerTitleStyle: {
              fontWeight: "bold",
            },
            headerTitleAlign: 'center',
          },
        },
    },
  )
  
export default ForgotPasswordNavigator;