import React from "react";
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

const WifiSetupScreen = props => {

    return (
      <View style={styles.screen}>
        <View style={{ position: "absolute", flex: 1, paddingRight: 10}}>
            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
                <View>
                    <Text style={{
                        fontSize: 31, fontFamily: 'euclid-triangle-semibold', textAlign: 'center', color: '#003459', bottom: 80
                        }}>Wi-Fi Setup</Text>
                    <Text style={{
                        fontSize: 18, fontFamily: 'euclid-triangle', fontWeight: "100", textAlign: 'center', color: 'grey', bottom: 70
                        }}>this feature is coming soon...</Text>
                </View>
            </View>
        </View>
      </View>
  )
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    backgroundColor: 'white',
    paddingBottom: 0,
  },
  buttonPressIn: {
    flexDirection: "row",
    flex: 1, 
    marginRight: 15,
    marginLeft: 30,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#00B2EE',
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: 290,
    height: 70,
  },
  buttonPressOut: {
    flexDirection: "row",
    flex: 1, 
    marginRight: 15,
    marginLeft: 30,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 0,
    backgroundColor: 'white',
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    width: 290,
    height: 70,
  }
});


export default WifiSetupScreen;