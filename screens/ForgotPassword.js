import React, { useReducer, useCallback, useEffect, useState } from 'react';
import {
  Alert,
  KeyboardAvoidingView,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Keyboard,
  ActivityIndicator,
  Platform,
  TouchableWithoutFeedback
} from 'react-native';
import Input from '../components/Input';
import { useDispatch } from 'react-redux';
import * as authActions from '../store/actions/auth';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const ForgotPassword = props => {

  const dispatch = useDispatch();
  const [errors, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
  }, [errors]);

  const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
      const updatedValues = {
        ...state.inputValues,
        [action.input]: action.value
      };
      const updatedValidities = {
        ...state.inputValidities,
        [action.input]: action.isValid
      };
      let updatedFormIsValid = true;
      for (const key in updatedValidities) {
        updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
      }
      return {
        formIsValid: updatedFormIsValid,
        inputValidities: updatedValidities,
        inputValues: updatedValues
      };
    }
    return state;
  };

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      username: ''
    },
    inputValidities: {
      username: false,
    },
    formIsValid: false
  });

  const forgotPasswordHandler = () => {
    setIsLoading(true);
    dispatch(authActions.getCode(formState.inputValues.username))
      .then((res) => {
        Alert.alert('Please check your email for confirmation')
        props.navigation.navigate('PasswordConfirmation', {username : formState.inputValues.username})
      })
      .catch((error) => {
        setError(error.message)
        setError()
      })
    setIsLoading(false);
  }

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      });
    },
    [dispatchFormState]
  );

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? "padding" : "height"}
        style={styles.screen}
      >
        <View>
          <Text style={{
            fontSize: 28, textAlign: 'center',
            color: '#003459', marginTop: 40, fontFamily: 'euclid-triangle-semibold'
          }}>Forgot your Password?</Text>
          <Text style={{
            fontSize: 15, textAlign: 'center',
            color: '#003459', fontFamily: 'euclid-triangle' }}>Let's start with your username...</Text>
        </View>
        <View style={styles.authContainer}>
            <Input
              id="username"
              label="Username"
              keyboardType="default"
              required
              autoCapitalize="none"
              errorText="Please enter a valid username."
              onInputChange={inputChangeHandler}
              initialValue=""
            />
            <View style={{ height: 40, marginBottom: 20 }}>
              {isLoading ? <ActivityIndicator style={{ paddingVertical: 30 }} /> :
              <TouchableOpacity underlayColor='#00ADEE'
                style={styles.customBtnBG} onPressIn={() => Keyboard.dismiss()} onPressOut={forgotPasswordHandler}>
                <View style={{ alignSelf: 'center' }}>
                  <Text style={{ fontSize: 20, textAlign: 'center', color: '#fff', 
                  fontFamily: 'euclid-triangle' }}>CONFIRM</Text>
                </View>
              </TouchableOpacity>}
            </View>
        </View>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  )
};

const styles = StyleSheet.create({  
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    width: '100%',
  },
    authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginTop: 20,
    marginBottom: 0,
    maxWidth: 280,
    maxHeight: 600,
    padding: 5
  },
  customBtnBG: {
    justifyContent: 'center',
    marginLeft: 0,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#00ADEE',
    borderRadius: 50,
    elevation: 1,
    width: 280,
    height: 50,
  }
});

export default ForgotPassword;
