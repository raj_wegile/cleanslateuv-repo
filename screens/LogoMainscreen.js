import React, { Component } from "react";
import {
  View,
  Image,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
// Load the app logo
const logo = require('../images/uvlogo1.png');
const background = require('../images/linesbackground3.png');

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    backgroundColor: 'white',
    paddingBottom: 0,
  },
  submit: {
    marginRight: 10,
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'white',
    borderRadius: 50,
    borderWidth: 2,
    borderColor: '#1261A0',
    width: 160,
    height: 50
  },
});

export default class LogoMainscreen extends Component {
  componentDidMount(){
      // Start counting when the page is loaded
      this.timeoutHandle = setTimeout(()=>{
      if (this.props.navigation.isFocused())
        this.props.navigation.navigate('MainPage')
      }, 2000);
  }

  componentWillUnmount(){
      clearTimeout(this.timeoutHandle);
  }

  render() {
    return (
        <ImageBackground 
        source={background}
        style={styles.screen}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('MainPage')}>
              <View>  
                <Image
                source={logo}
                style={{
                flex: 1,
                width: 250,
                height: 50,
                resizeMode: 'contain',
                bottom: 75
              }} 
                />
              </View>
          </TouchableOpacity>
      </ImageBackground>
      )
  }
}
