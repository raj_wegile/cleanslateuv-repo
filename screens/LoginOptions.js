import React, { Component } from "react";
import { Auth } from 'aws-amplify';
import {
    StyleSheet,
    View,
    Text,
    ActivityIndicator,
    BackHandler,
    Alert
  } from 'react-native'
import * as getDeviceActions from '../store/actions/getDevices';
import * as getCleanslateActions from '../store/actions/getCleanslates';
import * as getUserActions from '../store/actions/getUsers';
import { connect } from 'react-redux';
import { NavigationEvents } from 'react-navigation';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
  },
})

class LoginOptions extends Component {
    state = {
      userToken: null,
      firstName: null,
      lastName: null
    }
    async componentDidMount () {
      // If the login process exceeds the timeout then it navigates to the login screen
      this.timeoutHandle = setTimeout(()=>{
        if (this.props.navigation.isFocused()) {
          this.props.navigation.navigate('ManagerMain')
          Alert.alert('Error logging in: Please re-enter your credentials.')
        }
      // 10 second timeout until the user is kicked out of automatic sign-in
      // This should be adjusted if needed... but if sign-in exceeds 10 seconds
      // some optimization might be needed for pulling the user's CleanSlate's
      }, 10000);

      // Wait for the app to automatically login the user
      await this.loadApp()

      // Controls screen navigation if logged in/not
      if (this.props.navigation.isFocused()) {
        if (this.state.userToken)
          this.props.navigation.navigate('DepartManagerHome')
        else
          this.props.navigation.navigate('ManagerMain')
      }
    }

    loadApp = async () => {
      // Wait to get the current authenticated user
      await Auth.currentAuthenticatedUser()
      .then(async (user) => {
        // Set the state and attributes for the authenticated user
        this.setState({userToken: user.signInUserSession.accessToken.jwtToken})
        this.email = user.attributes.email;
        this.date = new Date();
        this.date.setHours(0, 0, 0, 0);

        const {dispatch} = this.props;
        this.userInformation = await dispatch(getUserActions.getUserInformation(this.email))
        this.setState({firstName: this.userInformation.userArray.body[0].Items[0].first_name})
        this.setState({lastName: this.userInformation.userArray.body[0].Items[0].last_name})

        // Check which group the user is apart of and navigate to the appropriate one
        if (user.signInUserSession.accessToken.payload['cognito:groups'][0] == "departmentManager") {
            // Dispatch the requests for department managers
            await dispatch(getDeviceActions.getDepartDeviceCount(this.email, this.date))
            await dispatch(getCleanslateActions.getDepartCleanslateCount(this.email, this.date))
        } else {
            // Dispatch the requests for hospital managers
            await dispatch(getDeviceActions.getHospiDeviceCount(this.email, this.date))
            await dispatch(getCleanslateActions.getHospiCleanslateCount(this.email, this.date))
        }
        // Send an update to the login count for the user
        dispatch(getUserActions.sendUserLogin(this.email))
      })
      .catch(err => {
        console.log(err)
      })
    }

    // Removes back button listener when screen is not focused
    onBlurr = () => {
      BackHandler.removeEventListener('hardwareBackPress',
      this.handleBackButtonClick);
    }
    
    // Adds back button listener when screen is focused
    onFocus = () => {
      BackHandler.addEventListener('hardwareBackPress', 
      this.handleBackButtonClick);
    }
      
    // Handles removing the back button functionality on the screen
    handleBackButtonClick = () => {
        if (this.props.navigation.isFocused())
            return true;
        return false;
    }

    render() {
      return (
        <View style={styles.container}>
          {/* Controls the back button functions are called when needed */}
          <NavigationEvents
            onWillFocus={this.onFocus}
            onWillBlur={this.onBlurr}
          />
          <Text style={{
                fontSize: 30, fontFamily: 'euclid-triangle-semibold', textAlign: 'center', color: '#003459'
              }}>Welcome</Text>
          <Text style={{
                fontSize: 25, fontFamily: 'euclid-triangle', fontWeight: "100", textAlign: 'center', color: 'grey', paddingBottom: 50
                }}>{this.state.firstName} {this.state.lastName}</Text>
          <ActivityIndicator size="large" color="#003459" />
        </View>     
      )
    }
  }

// Normal useDispatch() is not possible within a stateful component
// mapStateToProps is necessary to properly dispatch to the store
const mapStateToProps = null;
function mapDispatchToProps(useDispatch) {
    return {
        dispatch: (...args) => useDispatch(...args)
    };
}

// Exporting the state component using connect to associate the useDispatch() functionality
export default connect(mapStateToProps, mapDispatchToProps)(LoginOptions)