import React, { useState, useEffect, useCallback } from 'react';
import {
  StyleSheet,
  Alert,
  View,
  Text,
  FlatList,
  TouchableHighlight,
  BackHandler,
  Dimensions,
  RefreshControl
} from 'react-native';
import { ListItem, Card } from "react-native-elements";
import { useDispatch, useSelector } from 'react-redux';
import * as getCleanslateActions from '../../store/actions/getCleanslates';
import * as authActions from '../../store/actions/auth';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Button, Footer, FooterTab } from "native-base";
import { StackActions, NavigationActions } from 'react-navigation';

// Handles the reset for the navigation options, ensuring that navigation can properly proceed 
// if the user clicks the logout button by resetting the navigation counter to the MainNavigation route
const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'ManagerLoginOptions' })],
});

// This wraps the entirety of the Department Manager's screen
const DepartManagerScreen = props => {
  // Assigning constants for dispatching user information and error documentation
  const [err, setError] = useState();
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch();

  // This key extractor is used to generate the FlatList of
  // CleanSlate devices available to the user
  const keyExtractor = (item, index) => index.toString()

  // This holds the stateful information stored during the dispatch actions
  // Allows us to gather the array of CleanSlate's (departArray)
  let departArray = useSelector(state => state.departHospiCleanslate.departArray);

  // The errors and warnings variables store the count for the number of errors
  // and warnings exist in their respective arrays
  let errors = 0, warnings = 0;
  for (var i = 0; i < departArray.length; i++) {
    if (departArray[i].errorState != 'Normal')
      errors += 1
    // Using an else if so as not to count an occurence of both an error and warning
    else if (departArray[i].warningState != 'Normal')
      warnings += 1
  }

  // Calculating the percentage of devices in error/warning states reported to the user on the landing screen
  let percentage = Math.ceil((departArray.length-errors-warnings)/departArray.length*100);
  let email = departArray[0].email
  let date = new Date();
  date.setHours(0, 0, 0, 0);

  // Used to handle the generation of error messages
  // And control the hardware back buton functionality by preventing it on this screen
  useEffect(() => {
    if (err) {
      Alert.alert(err)
    }
    BackHandler.addEventListener('hardwareBackPress', onBackPress)
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', onBackPress)
  }, [err]);

  // Function used by the back handler requests to listen for the action
  const onBackPress = () => {
    if (props.navigation.isFocused())
      return true;
    return false;
  }

  // Usage count for the past day
  const countT = departArray.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.countT
  }, 0)

  // Usage count for the past 7 days
  const countP7 = departArray.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.countP7
  }, 0)

  // Usage count for the past 30 days
  const countP30 = departArray.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.countP30
  }, 0)

  // Usage count since deployment of the device
  const countAll = departArray.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.countAll
  }, 0)

  // When a specific CleanSlate is clicked on the FlatList, this constant is called
  // It is used to gather the necessary information about the specific cleanslate
  const getCleanslate = (csuv_serial) => {
    // Holds the data from departArray for one specific cleanslate
    const cleanslate = departArray.find(el => el.csuv_serial === csuv_serial);

    dispatch(getCleanslateActions.getCleanslate(cleanslate))
      .then((res) => {
        props.navigation.navigate('DMCleanslateScreen');
      })
      .catch((error) => {
        setError("Error, please contact CleanslatUV")
        setError()
      })
  }
  
  // Used to refresh the list of CleanSlate's available and reported to the user
  const refreshDMCleansate = async () => {
    await dispatch(getCleanslateActions.getDepartCleanslateCount(email, date))
    .then((res) => {
    })
    .catch((error) => {
      setError("Error, please contact CleanslatUV")
      setError()
    })
  }

  // Used to handle the actions after pull down to refresh is used
  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await refreshDMCleansate();
    setRefreshing(false);
  }, []);

  // Used to log the user out and navigate them back to the login page
  const logout = async () => {
    await dispatch(authActions.signOut())
      .then((res) => { props.navigation.dispatch(resetAction);
        props.navigation.navigate('ManagerMain');
      })
      .catch((error) => {
        setError("Error, please contact CleanslatUV")
        setError()
      })
  };

  // This constant is used to render the flatlist with each CleanSlate's
  // specific data
  const renderItem = ({ item }) => (
    <View style={{
      marginBottom: 10,
      marginTop: 10,
      marginLeft: 10,
      marginRight: 10,
      shadowColor: '#000',
      shadowOpacity: 1.8,
      shadowOffset: { width: 1, height: 2 },
      shadowRadius: 4,
      elevation: 5,
      borderRadius: 10,
      backgroundColor: 'white',
      overflow: 'hidden'
    }}>
      <ListItem
        title={
          <View>
            <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
              <View style={{ flex: 1, alignSelf: 'center' }}>
                <Text style={{ fontSize: 15, width: 50, color: '#003459', fontFamily: 'euclid-triangle' }}>
                  {item.csuv_serial.replace(/\s*\-\s*/g, "-\n")}</Text>
              </View>
              <View style={{ flex: 2, left: 10, alignSelf: 'center' }}>
                <Text style={{ fontSize: 15, width: 100, color: '#003459', fontFamily: 'euclid-triangle' }}
                >{item.location_name}</Text>
              </View>
              <TouchableHighlight
                style={{
                  flex: 1,
                  right: 0.045*Dimensions.get('window').width,
                  backgroundColor: item.color,
                  borderRadius: 10,
                  elevation: 1,
                  width: 65,
                  height: 25,
                  alignSelf: 'center'
                }}>
                <Text></Text>
              </TouchableHighlight>
              <View style={{ flex: 1, left: 0.015*Dimensions.get('window').width, alignSelf: 'center' }}>
                <Text style={{ fontSize: 15, color: '#003459', fontFamily: 'euclid-triangle' }}>
                  {item.countAll}</Text>
              </View>
              <FontAwesomeIcon name="caret-right" size={25} color="grey" style={{ alignSelf: 'center' }} />
            </View>
          </View>
        }
        onPress={() => { getCleanslate(item.csuv_serial) }}
        containerStyle={{
          borderRadius: 10,
          backgroundColor: 'white',
        }}
      >
        chevron={{ color: 'black', size: 25 }}
      </ListItem>
    </View>
  )

  return (
    <View style={styles.sheet}>
      <View style={{ flexDirection: "row", alignSelf: 'center' }}>
        <Card containerStyle={{
          flex: 1,
          justifyContent: 'space-between',
          alignItems: 'center',
          alignSelf: 'center',
          marginLeft: 5,
          marginRight: 10,
          width: '90%',
          backgroundColor: 'white',
          marginBottom: 10,
          marginTop: 10,
          maxWidth: 195,
          // 0.22 is not a special value merely achieved by experimental testing
          maxHeight: 0.22*Dimensions.get('window').height,
          borderRadius: 10,
          borderColor: 'white',
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowOpacity: 0.25,
          shadowRadius: 3,
          elevation: 10
          }}>
          <View style={{ paddingLeft: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: "row" }}>
            <Text style={{ fontFamily: 'euclid-triangle-semibold',
            fontSize: 80, color: percentage === 100 ? '#00ADEE' : 'red', marginTop: -5 }}>{percentage}</Text>
            <Text style={{ fontFamily: 'euclid-triangle-semibold', alignSelf: 'center',
            fontSize: 25, color: percentage === 100 ? '#00ADEE' : 'red', marginTop: -45 }}>%</Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
            <Text style={{ fontFamily: 'euclid-triangle', color: '#003459', marginTop: -20,
            fontSize: 16, flexWrap: 'wrap' }}>of your machines{'\n'}are in good health</Text>
          </View>
        </Card>
        <Card containerStyle={{
          flex: 1,
          justifyContent: 'space-between',
          alignItems: 'center',
          alignSelf: 'center',
          width: '90%',
          backgroundColor: 'white',
          marginLeft: -5,
          marginRight: 5,
          marginBottom: 10,
          marginTop: 10,
          maxWidth: 195,
          // 0.22 is not a special value merely achieved by experimental testing
          maxHeight: 0.22*Dimensions.get('window').height,
          borderRadius: 10,
          borderColor: 'white',
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowOpacity: 0.25,
          shadowRadius: 3,
          elevation: 10
          }}>
          <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'space-between',
            borderBottomWidth: 2,
            borderBottomColor: '#ECF3F7',
            marginBottom: 10
            }}>
            <Text style={{ color: '#003459', fontFamily: 'euclid-triangle', fontSize: 16 }}>
              Total Usage</Text>
            <Text style={{ fontSize: 18, fontFamily: 'euclid-triangle-semibold', 
            color: '#00ADEE', marginLeft: 20 }}>
              {countAll}</Text>
          </View>
          <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'space-between',
            borderBottomWidth: 2,
            borderBottomColor: '#ECF3F7',
            marginBottom: 10
            }}>
            <Text style={{ color: '#003459', fontSize: 16, fontFamily: 'euclid-triangle' }}>
              Past 30 Days</Text>
            <Text style={{ fontSize: 18, fontFamily: 'euclid-triangle-semibold', color: '#00ADEE', marginLeft: 20 }}>
              {countP30}</Text>
          </View>
          <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'space-between',
            borderBottomWidth: 2,
            borderBottomColor: '#ECF3F7',
            marginBottom: 10
            }}>
            <Text style={{ color: '#003459', fontSize: 16, fontFamily: 'euclid-triangle' }}>
              Past 7 Days</Text>
            <Text style={{ fontSize: 18, fontFamily: 'euclid-triangle-semibold', color: '#00ADEE', marginLeft: 20 }}>
              {countP7}</Text>
          </View>
          <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'space-between'
            }}>
            <Text style={{ color: '#003459', fontSize: 16, fontFamily: 'euclid-triangle' }}>
              Today</Text>
            <Text style={{ fontSize: 18, fontFamily: 'euclid-triangle-semibold', color: '#00ADEE', marginLeft: 20 }}>
              {countT}</Text>
          </View>
        </Card>
      </View>
      <View style={{ marginTop: 15, marginLeft: 5 }}>
        <View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1, justifyContent: 'flex-start', left: 20 }}>
              <Text style={{ color: '#003459', fontFamily: 'euclid-triangle-semibold',
                fontSize: 18,
              }}>S/N</Text>
            </View>
            <View style={{ flex: 1, justifyContent: "flex-start" }}>
              <View style={{ flex: 1 }}>
                <Text style={{ color: '#003459', fontFamily: 'euclid-triangle-semibold',
                  fontSize: 18
                }}>Location</Text>
              </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'flex-start', left: 10 }}>
              <Text style={{ color: '#003459', fontFamily: 'euclid-triangle-semibold',
                fontSize: 18
              }}>Status</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'flex-start' }}>
              <Text style={{ color: '#003459', fontFamily: 'euclid-triangle-semibold',
                fontSize: 18,
              }}>Usage</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.container}>
        <FlatList
            persistentScrollbar={true}
            keyExtractor={keyExtractor}
            data={departArray}
            renderItem={renderItem}
            style={styles.flatList}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
        />
      </View>
      <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
        <Footer>
          <FooterTab style={{ backgroundColor: "#FFF" }}>
            <Button
              useForeground
              vertical
              onPress={() => {}}>
              <FontAwesomeIcon name="home" size={25} color="grey" />
              <Text style={{
                fontSize: 10,
                color: 'grey',
                fontFamily: 'euclid-triangle'
              }}>HOME</Text>
            </Button>
          </FooterTab>
          <FooterTab style={{ backgroundColor: "#FFF"}}>
            <Button
                useForeground
                vertical
                onPress={() => { refreshDMCleansate() }}>
                <FontAwesomeIcon name="refresh" size={25} color="grey" />
                <Text style={{
                  fontSize: 10,
                  color: 'grey',
                  fontFamily: 'euclid-triangle'
                }}>REFRESH</Text>
              </Button>
          </FooterTab>
          <FooterTab style={{ backgroundColor: "#FFF"}}>
            <Button
                useForeground
                vertical
                onPress={() => { props.navigation.navigate('SetupMain') }}>
                <FontAwesomeIcon name="gear" size={25} color="grey" />
                <Text style={{
                  fontSize: 10,
                  color: 'grey',
                  fontFamily: 'euclid-triangle'
                }}>SETTINGS</Text>
              </Button>
          </FooterTab>
          <FooterTab style={{ backgroundColor: "white" }}>
            <Button
                useForeground
                vertical
                onPress={() => { logout() }}>
                <FontAwesomeIcon name="sign-out" size={25} color="grey" />
                <Text style={{
                  fontSize: 10,
                  color: 'grey',
                  fontFamily: 'euclid-triangle'
                }}>LOGOUT</Text>
              </Button>
          </FooterTab>
        </Footer>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
    sheet: {
      flex: 1
    },
    container: {
      flex: 1,
      paddingBottom: 20
    },
    flatList: {
      flexGrow: 0,
      marginBottom: 40,  
      shadowColor: '#000',  
      shadowOpacity: 0.15,  
      elevation: 1,
    },
  })

export default DepartManagerScreen