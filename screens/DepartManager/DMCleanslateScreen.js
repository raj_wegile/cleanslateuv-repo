import React, { useState, useEffect, useCallback } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Modal,
  Alert,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
  Linking
} from 'react-native';
import { Card } from "react-native-elements";
import * as getCleanslateActions from '../../store/actions/getCleanslates';
import * as authActions from '../../store/actions/auth';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {  Button, Footer, FooterTab,  } from "native-base";
import { useDispatch, useSelector } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';
import { VictoryAxis, VictoryBar, VictoryChart, VictoryTheme, VictoryLabel } from 'victory-native';

// Handles the reset for the navigation options, ensuring that navigation can properly proceed 
// if the user clicks the logout button by resetting the navigation counter to the MainNavigation route
const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'ManagerLoginOptions' })],
});

const DMCleanslateScreen = props => {

  // Constants that pull the stored information for the 
  // CleanSlate (set in the previous screen)
  const cleanslate = useSelector(state => state.cleanslate.cleanslate);

  // Determines whether the issue state should be set to error or warning
  // Based on the severity (prioritizes error states)
  let issue = (cleanslate.errorState == 'Normal') ? (cleanslate.warningState) : (cleanslate.errorState);

  // Constants that track whether the state of the modals used 
  // on this screen are visible
  const [modalVisible, setModalVisible] = useState(false);
  const [secondModalVisible, setSecondModalVisible] = useState(false);
  const [thirdModalVisible, setThirdModalVisible] = useState(false);

  // Constants that set the error and refreshing states
  const [errors, setError] = useState();
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch();

  // Constant used to store the current date for
  // updating data on the screen
  const date = new Date();
  date.setHours(0, 0, 0, 0) 
  
  // Used to handle the generation of error messages
  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
  }, [errors]);

  // Used to log the user out and navigate them back to the login page
  const logout = async () => {
    await dispatch(authActions.signOut())
      .then((res) => { props.navigation.dispatch(resetAction);
        props.navigation.navigate('ManagerMain');
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  };

  // Used to refresh the data pulled for the specific CleanSlate visible to the user
  const getCleanslate = async (csuv_serial, email, date) => {
    await dispatch(getCleanslateActions.getDepartCleanslateCount(email, date))
      .then(async (res1) => {
        // Pulling the data for this specific cleanslate
        const cleanslate = res1.departArray.find(el => el.csuv_serial === csuv_serial);

        await dispatch(getCleanslateActions.getCleanslate(cleanslate))
        .then((res) => {
        })
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  }

  // Used to handle the actions after pull down to refresh is used
  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await getCleanslate(cleanslate.csuv_serial, cleanslate.email, date);
    setRefreshing(false);
  }, []);

  // Constant used to hold the data presented to users
  // on the graph (weekly sanitization counts)
  const data = [
    {week: 1, count: cleanslate.countW4},
    {week: 2, count: cleanslate.countW3},
    {week: 3, count: cleanslate.countW2},
    {week: 4, count: cleanslate.countP7}
  ];

  return (
    <View style={styles.container}>
      <ScrollView style={styles.container} 
        refreshControl={ <RefreshControl refreshing={refreshing} onRefresh={onRefresh} /> }>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 22
          }}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}>
          <View style={styles.modalView}>
            <View style={{
              alignSelf: 'flex-start',
              flexDirection: 'row',
              alignItems: 'flex-start',
              height: 30
            }}>
              <TouchableHighlight
                style={{
                  flexDirection: "row",
                  justifyContent: 'flex-start',
                  backgroundColor: 'green',
                  borderRadius: 5,
                  shadowOpacity: 0.1,
                  shadowRadius: 3,
                  elevation: 3,
                  width: 80,
                  height: 20,
                }}>
                <Text></Text>
              </TouchableHighlight>
              <Text style={styles.modalText}>Good Condition</Text>
            </View>
            <View style={{
              alignSelf: 'flex-start',
              flexDirection: 'row',
              alignItems: 'flex-start',
              height: 30
            }}>
              <TouchableHighlight
                style={{
                  flexDirection: "row",
                  justifyContent: 'flex-start',
                  backgroundColor: 'orange',
                  borderRadius: 5,
                  shadowOpacity: 0.1,
                  shadowRadius: 3,
                  elevation: 3,
                  width: 80,
                  height: 20,
                }}
              >
                <Text></Text>
              </TouchableHighlight>
              <Text style={styles.modalText}>Needs Service Soon</Text>
            </View>
            <View style={{
              alignSelf: 'flex-start',
              flexDirection: 'row',
              alignItems: 'flex-start',
              height: 30
            }}>
              <TouchableHighlight
                style={{
                  flexDirection: "row",
                  justifyContent: 'flex-start',
                  backgroundColor: 'red',
                  borderRadius: 5,
                  shadowOpacity: 0.1,
                  shadowRadius: 3,
                  elevation: 3,
                  width: 80,
                  height: 20,
                }}
              >
                <Text></Text>
              </TouchableHighlight>
              <Text style={styles.modalText}>Needs Service Immediately</Text>
            </View>
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#00ADEE" }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyle}>OK</Text>
            </TouchableHighlight>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={thirdModalVisible}
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 22
          }}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={{ color: '#003459', marginLeft: 10, marginBottom: 10, textAlign: "center", fontSize: 15, fontFamily: 'euclid-triangle-semibold' }}>Contact CleanSlate UV immediately</Text>
              <Text onPress={() => { Linking.openURL('tel:+1 (877) 553-6778 2') }} style={styles.modalText}>@ +1 (877) 553-6778 Ext 2</Text>
              <Text onPress={() => { Linking.openURL('mailto:engineering@cleanslateuv.com') }} style={styles.modalText}>@ engineering@cleanslateuv.com</Text>
              <TouchableHighlight
                style={{ ...styles.openButton, backgroundColor: "#00ADEE" }}
                onPress={() => {
                  setThirdModalVisible(!thirdModalVisible);
                }}
              >
                <Text style={styles.textStyle}>Contact</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          visible={secondModalVisible}
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 22
          }}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={{ color: '#003459', marginLeft: 10, marginBottom: 10, textAlign: "center", fontSize: 15, fontFamily: 'euclid-triangle-semibold' }}>Contact CleanSlate UV soon</Text>
              <Text onPress={() => { Linking.openURL('tel:+1 (877) 553-6778 2') }} style={styles.modalText}>@ +1 (877) 553-6778 Ext 2</Text>
              <Text onPress={() => { Linking.openURL('mailto:engineering@cleanslateuv.com') }} style={styles.modalText}>@ engineering@cleanslateuv.com</Text>
              <TouchableHighlight
                style={{ ...styles.openButton, backgroundColor: "#00ADEE" }}
                onPress={() => {
                  setSecondModalVisible(!secondModalVisible);
                }}
              >
                <Text style={styles.textStyle}>Contact</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>
        <View style={{ flexDirection: "row", alignSelf: 'center' }}>
          <Card containerStyle={{
            marginLeft: 20,
            marginRight: 0,
            width: '90%',
            height: '100%',
            backgroundColor: 'white',
            marginBottom: 10,
            marginTop: 20,
            maxWidth: 100,
            maxHeight: 80,
            borderRadius: 10,
            borderColor: 'white',
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 10,
            elevation: 20
            }}>
              <View style={{ justifyContent: 'center', alignSelf: 'center', flexDirection: "row" }}>
                <Text style={{ fontFamily: 'euclid-triangle',
                fontSize: 18, color: '#00ADEE' }}>{cleanslate.csuv_serial.replace(/\s*\-\s*/g, "-\n")}</Text>
              </View>
          </Card>
          <Card containerStyle={{
            flex: 1,
            justifyContent: 'center',
            width: '90%',
            height: '100%',
            backgroundColor: 'white',
            marginLeft: 5,
            marginRight: 20,
            marginBottom: 10,
            marginTop: 20,
            maxWidth: 265,
            maxHeight: 80,
            borderRadius: 10,
            borderColor: 'white',
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 5,
            elevation: 10
            }}>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'center' }}>
                <Text style={{ fontSize: 18, fontFamily: 'euclid-triangle-semibold',
                color: '#00ADEE' }}>{cleanslate.departmentName}: {cleanslate.location_name}</Text>
              </View>
          </Card>
        </View>
        <View>
          <Card containerStyle={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            width: '90%',
            height: '100%',
            backgroundColor: 'white',
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 10,
            marginTop: 10,
            maxWidth: 500,
            maxHeight: 170,
            borderRadius: 10,
            borderColor: 'white',
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 5
            },
            shadowOpacity: 0.25,
            shadowRadius: 5,
            elevation: 10
          }}>
            <View style={{
              flex: 1, flexDirection: 'row',
              justifyContent: 'center',
              borderBottomWidth: 2, alignSelf: 'center',
              borderBottomColor: '#ECF3F7',
              marginBottom: 5, marginTop: 5
            }}>
              <View style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-start' }}>
                <Text style={{ fontSize: 16, fontFamily: 'euclid-triangle',
                    color: '#003459'
                }}>Status</Text>
                <FontAwesomeIcon name="question-circle-o"
                  size={20} color='#00ADEE'
                  onPress={() => setModalVisible(!modalVisible)} />
              </View>
              <View style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-end' }}>
                <TouchableHighlight
                  style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    backgroundColor: cleanslate.color,
                    borderRadius: 5,
                    shadowOpacity: 0.15,
                    shadowRadius: 5,
                    elevation: 5,
                    maxWidth: 0.24*Dimensions.get('window').width,
                    height: 20,
                  }}>
                  <Text></Text>
                </TouchableHighlight>
              </View>
            </View>
            <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'center',
            borderBottomWidth: 2,
            borderBottomColor: '#ECF3F7',
            marginBottom: 5, marginTop: 5
            }}>
              <View style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-start' }}>
                <Text style={{ fontSize: 16, fontFamily: 'euclid-triangle',
                    color: '#003459'
                }}>Bulb Order Due</Text>
              </View>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                  <Text style={{ textAlign: 'right', fontSize: 16, fontFamily: 'euclid-triangle-semibold', color: '#003459' }}>
                    {cleanslate.order.substr(0, 10).replace(/-/g, '/')}</Text>
                </View>
              </View>
            </View>
            <View style={{
              flex: 1, flexDirection: 'row',
              justifyContent: 'center',
              borderBottomWidth: 2,
              borderBottomColor: '#ECF3F7',
              marginBottom: 5, marginTop: 5
              }}>
                <View style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-start' }}>
                  <Text style={{ fontSize: 16, fontFamily: 'euclid-triangle',
                      color: '#003459'
                  }}>Bulb Change Due</Text>
                </View>
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                    <Text style={{ textAlign: 'right', fontSize: 16, fontFamily: 'euclid-triangle-semibold', color: '#003459' }}>
                      {cleanslate.change.substr(0, 10).replace(/-/g, '/')}</Text>
                  </View>
                </View>
            </View>
            <View style={{
              flex: 2, flexDirection: 'row',
              justifyContent: 'center',
              flexGrow: 2, marginBottom: -15, marginTop: -5
            }}>
              <Text style={{ flex: 1, fontSize: 16, fontFamily: 'euclid-triangle', color: '#003459', 
              alignSelf: 'center' }}>
                Machine Health</Text>
              <Text style={{ flex: 1, textAlign: 'right', flexWrap: 'wrap', fontSize: 16, marginLeft: 40,
              fontFamily: 'euclid-triangle-semibold', color: '#003459', alignSelf: 'center' }}>
                {issue}</Text>
            </View>
          </Card>
        </View>
        <View>
          <View>
            <Card containerStyle={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              width: '90%',
              height: '100%',
              backgroundColor: 'white',
              marginLeft: 20,
              marginRight: 20,
              marginTop: 10,
              maxWidth: 500,
              maxHeight: 170,
              borderRadius: 10,
              borderColor: 'white',
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 5
              },
              shadowOpacity: 0.25,
              shadowRadius: 5,
              elevation: 10
            }}>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'center',
                borderBottomWidth: 2,
                borderBottomColor: '#ECF3F7',
                marginBottom: 5, marginTop: 5,
                flexGrow: 1
              }}>
                <View style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-start' }}>
                    <Text style={{ fontSize: 16, fontFamily: 'euclid-triangle',
                      color: '#003459'
                    }}>Since Deployment</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                  <Text style={{ textAlign: 'right', fontSize: 18, fontFamily: 'euclid-triangle-semibold', color: '#003459' }}>
                    {cleanslate.countAll}</Text>
                </View>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'center',
                borderBottomWidth: 2, alignSelf: 'center',
                borderBottomColor: '#ECF3F7',
                marginBottom: 5, marginTop: 5,
                flexGrow: 1
                }}>
                <View style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-start' }}>
                  <Text style={{ fontSize: 16, fontFamily: 'euclid-triangle',
                    color: '#003459'
                  }}>Past 30 Days</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                  <Text style={{ textAlign: 'right', fontSize: 18, fontFamily: 'euclid-triangle-semibold', color: '#003459' }}>
                    {cleanslate.countP30}</Text>
                </View>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'center',
                borderBottomWidth: 2, alignSelf: 'center',
                borderBottomColor: '#ECF3F7',
                marginBottom: 5, marginTop: 5,
                flexGrow: 1
                }}>
                  <View style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-start' }}>
                    <Text style={{ fontSize: 16, fontFamily: 'euclid-triangle',
                      color: '#003459'
                    }}>Past 7 Days</Text>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                    <Text style={{ textAlign: 'right', fontSize: 18, fontFamily: 'euclid-triangle-semibold', color: '#003459' }}>
                      {cleanslate.countP7}</Text>
                  </View>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'center',
                alignSelf: 'center',
                marginBottom: 5, marginTop: 5,
                flexGrow: 1
              }}>
                <View style={{ flex: 1, flexDirection: "row", justifyContent: 'flex-start' }}>
                  <Text style={{ fontSize: 16, fontFamily: 'euclid-triangle',
                    color: '#003459'
                  }}>Today</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                  <Text style={{ textAlign: 'right', fontSize: 18, fontFamily: 'euclid-triangle-semibold', color: '#003459' }}>
                    {cleanslate.countT}</Text>
                </View>
              </View>
            </Card>
          </View>
          <View>
            { // Checking if the cleanslate has experienced a warning
            (cleanslate.color == 'orange') ? (<View><TouchableOpacity
              style={styles.LoginButton}
              onPress={() => setSecondModalVisible(!secondModalVisible)} >
              <View>
                <Text style={{
                  paddingTop: 7, textAlign: 'center', fontSize: 20,
                  color: 'white', fontFamily: 'euclid-triangle-semibold',
                }}>Bulb Change</Text>
              </View>
            </TouchableOpacity></View>) : (<View></View>)}
          </View>
          <View>
            { // Checking if the cleanslate has experienced an error
            (cleanslate.color == 'red') ? (<View><TouchableOpacity
              style={styles.LoginButton}
              onPress={() => setThirdModalVisible(!thirdModalVisible)} >
              <View >
                <Text style={{
                  paddingTop: 7, textAlign: 'center', fontSize: 20,
                  color: 'white', fontFamily: 'euclid-triangle-semibold'
                }}>Contact CleanSlate UV</Text>
              </View>
            </TouchableOpacity></View>) : (<View></View>)}
          </View>
          <Card containerStyle={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              width: '90%',
              height: '100%',
              backgroundColor: 'white',
              marginLeft: 20,
              marginRight: 20,
              marginTop: 20,
              // 0.12 is not a special value
              // it is merely obtained through experimentation
              marginBottom: 0.12*Dimensions.get('window').height,
              maxWidth: 500,
              maxHeight: 340,
              borderRadius: 10,
              borderColor: 'white',
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 5
              },
              shadowOpacity: 0.25,
              shadowRadius: 5,
              elevation: 10
            }}>
            <VictoryChart
              // Adjusting the graph width based on the user's screen
              theme={VictoryTheme.material}
              domainPadding={{ x: 35, y: 0 }}
              width={Dimensions.get('window').width-20}
            >
              <VictoryLabel text="Device Sanitizations/Week" 
              // 0.5 is not a special value, it is merely obtained through experimentation
              x={0.5*Dimensions.get('window').width} y={30} textAnchor="middle" color='#003459'
                style={{ fontSize: 18, fill: '#003459' }} />
              <VictoryAxis
                tickValues={[1, 2, 3, 4]}
                // Adding spacing and newlines to ensure axis looks optimal
                tickFormat={[cleanslate.dateW4+' -\n'+cleanslate.dateW3+'  ', cleanslate.dateW3+' -\n'+cleanslate.dateW2+'  ', cleanslate.dateW2+' -\n'+cleanslate.dateW1+'  ', cleanslate.dateW1+' -\n'+cleanslate.dateNow+'  ']}
                style={{
                  tickLabels: { textAnchor: 'middle' },
                  grid: { stroke: "transparent" },
                }}
              />
              <VictoryAxis
                tickFormat={(t) => (cleanslate.countP7 === 0 && cleanslate.countW2 === 0 && cleanslate.countW3 === 0 && cleanslate.countW4 === 0) ? '' : `${t}` }
                dependentAxis
                style={{
                  tickLabels: { padding: 5 },
                  grid: { stroke: "transparent" },
                }}
              />
              <VictoryBar
                barRatio={1}
                data={data}
                x="week"
                y="count"
                // Animation duration for the graph to appear
                animate={{ duration: 300 }}
                style={{ 
                  data: {
                    // Changing the fill on the data depending on which index it is
                    fill: ({ datum }) => datum.x % 2 == 0 ? "#000000" : "#00ADEE",
                    fillOpacity: 0.7
                  },
                  labels: { 
                    fontColor: '#003459'
                  } 
                }}
              />
            </VictoryChart>
          </Card>
        </View>
      </ScrollView>
      <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
        <Footer>
          <FooterTab style={{ backgroundColor: "#FFF" }}>
            <Button
              // Using foreground to ensure button has feedback on press for android devices
              useForeground
              vertical
              onPress={() => {props.navigation.navigate('DepartManagerHome')}}>
              <FontAwesomeIcon name="home" size={25} color="grey" />
              <Text style={{
                fontSize: 10,
                color: 'grey',
                fontFamily: 'euclid-triangle'
              }}>HOME</Text>
            </Button>
          </FooterTab>
          <FooterTab style={{ backgroundColor: "#FFF" }}>
            <Button
              // Using foreground to ensure button has feedback on press for android devices
              useForeground
              vertical
              onPress={() => { getCleanslate(cleanslate.csuv_serial, cleanslate.email, date) }}>
              <FontAwesomeIcon name="refresh" size={25} color="grey" />
              <Text style={{
                fontSize: 10,
                color: 'grey',
                fontFamily: 'euclid-triangle'
              }}>REFRESH</Text>
            </Button>
          </FooterTab>
          <FooterTab style={{ backgroundColor: "#FFF"}}>
            <Button
              // Using foreground to ensure button has feedback on press for android devices
              useForeground
              vertical
              onPress={() => { props.navigation.navigate('SetupMain') }}>
              <FontAwesomeIcon name="gear" size={25} color="grey" />
              <Text style={{
                fontSize:10,
                color: 'grey',
                fontFamily: 'euclid-triangle'
              }}>SETTINGS</Text>
            </Button>
          </FooterTab>
          <FooterTab style={{ backgroundColor: "white" }}>
            <Button
              // Using foreground to ensure button has feedback on press for android devices
              useForeground
              vertical
              onPress={() => { logout() }}>
              <FontAwesomeIcon name="sign-out" size={25} color="grey" />
              <Text style={{
                fontSize: 10,
                color: 'grey',
                fontFamily: 'euclid-triangle'
              }}>LOGOUT</Text>
            </Button>
          </FooterTab>
        </Footer>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    flexGrow: 0,
    marginBottom: 50
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "flex-start",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    alignSelf: 'center',
    backgroundColor: "#00B2EE",
    borderRadius: 20,
    marginTop: 10,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    textAlign: "center",
    fontFamily: 'euclid-triangle-semibold'
  },
  modalText: {
    color: '#003459',
    marginLeft: 10,
    textAlign: "center",
    fontSize: 15,
    fontFamily: 'euclid-triangle'
  },
  LoginButton: {
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#00B2EE',
    borderRadius: 50,
    elevation: 1,
    width: 300,
    height: 60,
  },
})


export default DMCleanslateScreen