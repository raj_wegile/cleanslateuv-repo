import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Text,
} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

const background = require('../images/linesbackground5.png')

export default class SetupScreen extends Component {
    state = {
        setBluetooth: false,
        setWifi: false,
        setFirmware: false,
        setDownload: false
    }

    render() {
        return (
        <View style={styles.screen}>
            <ImageBackground 
            source={background}
            style={styles.screen}>
                <View style={{ position: "absolute", flex: 1, paddingRight: 10}}>
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}> 
                            <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate('BluetoothSetup') }} >
                                <View style={styles.buttonPressOut}>
                                    <View style={{ paddingLeft: 8, paddingRight: 14, alignSelf: 'center' }} >
                                        <FontAwesomeIcon name="mobile" size={50} color='#003459' />
                                    </View>
                                    <View style={{ flexDirection: "column", flex: 1, alignSelf: 'center' }}>
                                        <Text style={{ textAlign: 'left', fontSize: 18, color: '#003459', 
                                        fontFamily: 'euclid-triangle-semibold' }}>Setup Bluetooth</Text>
                                        <Text style={{ textAlign: 'left', fontSize: 12, color: '#003459', 
                                        fontFamily: 'euclid-triangle' }}>Connect to CleanSlate via Bluetooth</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}> 
                            <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate('WifiSetup') }} >
                                <View style={styles.buttonPressOut}>
                                    <View style={{ paddingLeft: 2, paddingRight: 8, alignSelf: 'center' }} >
                                        <FontAwesomeIcon name="wifi" size={30} color='#003459' />
                                    </View>
                                    <View style={{ flexDirection: "column", flex: 1, alignSelf: 'center' }}>
                                        <Text style={{ textAlign: 'left', fontSize: 18, color: '#003459', 
                                        fontFamily: 'euclid-triangle-semibold' }}>Setup Wi-Fi</Text>
                                        <Text style={{ textAlign: 'left', fontSize: 12, color: '#003459', 
                                        fontFamily: 'euclid-triangle' }}>Connect CleanSlate to Wi-Fi</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}> 
                            <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate('BluetoothDownload') }} >
                                <View style={styles.buttonPressOut}>
                                    <View style={{ paddingLeft: 0, paddingRight: 7, alignSelf: 'center' }} >
                                        <FontAwesomeIcon name="retweet" size={35} color='#003459' />
                                    </View>
                                    <View style={{ flexDirection: "column", flex: 1, alignSelf: 'center' }}>
                                        <Text style={{ textAlign: 'left', fontSize: 18, color: '#003459', 
                                        fontFamily: 'euclid-triangle-semibold' }}>Download Data</Text>
                                        <Text style={{ textAlign: 'left', fontSize: 12, color: '#003459', 
                                        fontFamily: 'euclid-triangle' }}>Sync CleanSlate with the cloud</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}> 
                            <TouchableOpacity
                                onPress={() => { this.props.navigation.navigate('FirmwareUpdate') }} >
                                <View style={styles.buttonPressOut}>
                                    <View style={{ paddingLeft: 2, paddingRight: 10, alignSelf: 'center' }} >
                                        <FontAwesomeIcon name="cloud-download" size={30} color='#003459' />
                                    </View>
                                    <View style={{ flexDirection: "column", flex: 1, alignSelf: 'center'}}>
                                        <Text style={{ textAlign: 'left', fontSize: 18, color: '#003459', 
                                        fontFamily: 'euclid-triangle-semibold' }}>Update Firmware</Text>
                                        <Text style={{ textAlign: 'left', fontSize: 12, color: '#003459', 
                                        fontFamily: 'euclid-triangle' }}>Install new CleanSlate Firmware</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        </View>
    )}
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    backgroundColor: 'white'
  },
  buttonPressOut: {
    flexDirection: "row",
    flex: 1, 
    marginRight: 15,
    marginLeft: 30,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 0,
    backgroundColor: 'white',
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    width: 290,
    height: 70,
  }
});
