import React, { useReducer, useCallback, useEffect, useState } from 'react';
import {
  Alert,
  KeyboardAvoidingView,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Keyboard,
  ActivityIndicator,
  Platform,
  TouchableWithoutFeedback
} from 'react-native';
import Input from '../components/Input';
import { useDispatch } from 'react-redux';
import * as authActions from '../store/actions/auth';
import { StackActions, NavigationActions } from 'react-navigation';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'ManagerLoginOptions' })],
});

const PasswordConfirmation = props => {

  const dispatch = useDispatch();
  const [errors, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
  }, [errors]);

  const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
      const updatedValues = {
        ...state.inputValues,
        [action.input]: action.value
      };
      const updatedValidities = {
        ...state.inputValidities,
        [action.input]: action.isValid
      };
      let updatedFormIsValid = true;
      for (const key in updatedValidities) {
        updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
      }
      return {
        formIsValid: updatedFormIsValid,
        inputValidities: updatedValidities,
        inputValues: updatedValues
      };
    }
    return state;
  };

  const [formState] = useReducer(formReducer, {
    inputValues: {
      username: props.navigation.getParam('username', '')
    },
    inputValidities: {
      username: false,
    },
    formIsValid: false
  });

  const [submitFormState, dispatchsubmitFormState] = useReducer(formReducer, {
    inputValues: {
      oneTimeCode: '',
      password: '',
    },
    inputValidities: {
      oneTimeCode: false,
      password: false,
    },
    formIsValid: false
  });

  const newPasswordHandler = () => {
    setIsLoading(true);
    if (submitFormState.inputValues.password == submitFormState.inputValues.newPassword) {
        dispatch(authActions.forgotPasswordSubmit(formState.inputValues.username, submitFormState.inputValues.oneTimeCode, submitFormState.inputValues.password))
        .then((res) => {
          props.navigation.dispatch(resetAction);
          props.navigation.navigate('ManagerMain');
        })
        .catch((error) => {
            setError(error.message)
            setError()
        })
    }
    else {
        Alert.alert('Error, passwords do not match!')
        setError()
    }
    setIsLoading(false);
  }

  const inputChangeHandlerReset = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchsubmitFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      });
    },
    [dispatchsubmitFormState]
  );

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <KeyboardAvoidingView
        behavior="padding"
        keyboardVerticalOffset={Platform.OS === 'ios' ? 160 : -100}
        style={styles.screen}
      >
        <View>
          <Text style={{
            fontSize: 28, textAlign: 'center',
            color: '#003459', marginTop: 60, fontFamily: 'euclid-triangle-semibold'
          }}>Password Confirmation</Text>
          <Text style={{
            fontSize: 15, textAlign: 'center',
            color: '#003459', fontFamily: 'euclid-triangle' }}>Enter your new credentials...</Text>
        </View>
        <View style={styles.authContainer}>
            <Input
              id="oneTimeCode"
              label="Confirmation Code"
              keyboardType="default"
              required
              autoCapitalize="none"
              errorText="Please enter a valid code."
              onInputChange={inputChangeHandlerReset}
              initialValue=""
            />
            <Input
              id="password"
              label="New Password"
              keyboardType="default"
              secureTextEntry
              required
              minLength={5}
              autoCapitalize="none"
              errorText="Please enter a valid password."
              onInputChange={inputChangeHandlerReset}
              initialValue=""
            />
            <Input
              id="newPassword"
              label="Confirm New Password"
              keyboardType="default"
              secureTextEntry
              required
              minLength={5}
              autoCapitalize="none"
              errorText="Passwords do not match."
              onInputChange={inputChangeHandlerReset}
              initialValue=""
            />
            <View style={{ height: 40 }}>
              {isLoading ? <ActivityIndicator style={{ paddingVertical: 30 }} /> :
              <TouchableOpacity underlayColor='#0286c2'
                style={styles.customBtnBG} onPressIn={() => Keyboard.dismiss()} onPressOut={newPasswordHandler}>
                <View style={{ alignSelf: 'center' }}>
                  <Text style={{ fontSize: 18, textAlign: 'center', color: '#fff', 
                  fontFamily: 'euclid-triangle' }}>RESET PASSWORD</Text>
                </View>
              </TouchableOpacity>}
            </View>
        </View>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  )
};

const styles = StyleSheet.create({  
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    width: '100%',
  },
    authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginTop: 20,
    marginBottom: 0,
    maxWidth: 280,
    maxHeight: 600,
    padding: 5
  },
  customBtnBG: {
    justifyContent: 'center',
    marginLeft: 0,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#00ADEE',
    borderRadius: 50,
    elevation: 1,
    width: 280,
    height: 50,
  }
});

export default PasswordConfirmation;
