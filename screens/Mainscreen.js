import React, { useState, useEffect } from "react";
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  BackHandler
} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";

const background = require('../images/linesbackground4.png');

const Mainscreen = props => {

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', onBackPress)
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', onBackPress)
  }, [])

  const onBackPress = () => {
    if (props.navigation.isFocused())
      return true;
    return false;
  }

  const [login, setLogin] = useState(false);
  const [setup, setSetup] = useState(false);

  return (
    <ImageBackground
      source={background}
      style={styles.screen}>
      <View style={{ position: "absolute", flex: 1, paddingTop: 10, paddingRight: 10}}>
        <View style={{ bottom: 40, flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
        <View style={{ flex: 1, flexDirection: 'row'}}> 
          <TouchableOpacity
            onPress={() => { setLogin(true), setSetup(false), props.navigation.navigate('ManagerLoginOptions') }}>
            <View>
              {login? (<View style={{ flexDirection: "row", flex: 1, marginRight: 15,
                  marginLeft: 30,
                  marginTop: 10,
                  marginBottom: 20,
                  paddingTop: 8,
                  paddingBottom: 10,
                  paddingLeft: 10,
                  paddingRight: 10,
                  backgroundColor: '#00ADEE',
                  borderRadius: 10,
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  elevation: 5,
                  width: 290,
                  height: 70,
                 }}>
                <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 12 }} >
                  <FontAwesomeIcon name="user-circle" size={30} color='white' />
                </View>
                <View style={{ flexDirection: "column", flex: 1 }}>
                  <Text style={{ textAlign: 'left', fontSize: 20, color: 'white',
                  fontFamily: 'euclid-triangle-semibold' }}>Login</Text>
                  <Text style={{ textAlign: 'left', fontSize: 15, color: 'white', 
                  fontFamily: 'euclid-triangle' }}>Enterprise Suite</Text>
                </View>
              </View>) : (<View style={{ 
                  flexDirection: "row", flex: 1, marginRight: 15,
                  marginLeft: 30,
                  marginTop: 10,
                  marginBottom: 20,
                  paddingTop: 8,
                  paddingBottom: 10,
                  paddingLeft: 10,
                  paddingRight: 0,
                  backgroundColor: 'white',
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  elevation: 5,
                  borderRadius: 10,
                  width: 290,
                  height: 70,
                  }}>
                <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 12 }} >
                  <FontAwesomeIcon name="user-circle" size={30} color='#003459' />
                </View>
                <View style={{ flexDirection: "column", flex: 1 }}>
                  <Text style={{ textAlign: 'left', fontSize: 20, color: '#003459', 
                  fontFamily: 'euclid-triangle-semibold' }}>Login</Text>
                  <Text style={{ textAlign: 'left', fontSize: 15, color: '#003459', 
                  fontFamily: 'euclid-triangle' }}>Enterprise Suite</Text>
                </View>
              </View>)}
            </View>
          </TouchableOpacity>
          </View>
          <View style={{ flex: 1, flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => { setSetup(true), setLogin(false), props.navigation.navigate('SetupMain') }}>
            <View>
              {setup? (<View style={{ flexDirection: "row", flex: 1, marginRight: 15,
                  marginLeft: 30,
                  marginTop: 10,
                  marginBottom: 20,
                  paddingTop: 8,
                  paddingBottom: 10,
                  paddingLeft: 10,
                  paddingRight: 10,
                  backgroundColor: '#00ADEE',
                  borderRadius: 10,
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  elevation: 5,
                  width: 290,
                  height: 70,
                 }}>
                <View style={{ paddingLeft: 10, paddingRight: 5, paddingTop: 10}} >
                  <FontAwesomeIcon name="gear" size={35} color='white' />
                </View>
                <View style={{ flexDirection: "column", flex: 1, marginLeft: 5 }}>
                  <Text style={{ textAlign: 'left', fontSize: 20, color: 'white', 
                  fontFamily: 'euclid-triangle-semibold' }}>Setup</Text>
                  <Text style={{ textAlign: 'left', fontSize: 15, color: 'white', 
                  fontFamily: 'euclid-triangle' }}>Wireless Connectivity</Text>
                </View>
              </View>) : (<View style={{ flexDirection: "row", flex: 1, marginRight: 15,
                  marginLeft: 30,
                  marginTop: 10,
                  marginBottom: 20,
                  paddingTop: 8,
                  paddingBottom: 10,
                  paddingLeft: 10,
                  paddingRight: 10,
                  backgroundColor: 'white',
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  elevation: 5,
                  borderRadius: 10,
                  width: 290,
                  height: 70,
                  }}>
                <View style={{ paddingLeft: 10, paddingRight: 5, paddingTop: 10}} >
                  <FontAwesomeIcon name="gear" size={35} color='#003459' />
                </View>
                <View style={{ flexDirection: "column", flex: 1, marginLeft: 5 }}>
                  <Text style={{ textAlign: 'left', fontSize: 20, color: '#003459', 
                  fontFamily: 'euclid-triangle-semibold' }}>Setup</Text>
                  <Text style={{ textAlign: 'left', fontSize: 15, color: '#003459', 
                  fontFamily: 'euclid-triangle' }}>Wireless Connectivity</Text>
                </View>
              </View>)}
            </View>
          </TouchableOpacity>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    backgroundColor: 'white',
    paddingBottom: 0,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  openButton: {
    backgroundColor: "#00ADEE",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontFamily: 'euclid-triangle-semibold',
    textAlign: "center"
  },
  GetStartedButton: {
    flexDirection: "row", 
    flex: 1,
    justifyContent: 'center',
    marginLeft: 30,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#00ADEE',
    borderRadius: 50,
    elevation: 1,
    width: 290,
    height: 60,
  },
});

export default Mainscreen;
