import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Alert,
  Text,
  FlatList,
  TouchableHighlight,
  Modal,
} from 'react-native';
import { ListItem, Card } from "react-native-elements";
import { Button, Footer, FooterTab } from "native-base";
import { useDispatch, useSelector } from 'react-redux';
import * as authActions from '../../store/actions/auth';
import * as getCleanslateActions from '../../store/actions/getCleanslates';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';


const HMCleanslateScreen = props => {

  const [modalVisible, setModalVisible] = useState(false);
  const [err, setError] = useState();
  const dispatch = useDispatch();
  const departArrary = useSelector(state => state.departmentCleanslate.departGroup);
  let name = departArrary[0].departmentName
  let email = departArrary[0].email;
  let date = new Date();
  date.setHours(0, 0, 0, 0);
  const keyExtractor = (item, index) => index.toString()
  const countT = departArrary.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.countT
  }, 0)
  const countP7 = departArrary.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.countP7
  }, 0)
  const countP30 = departArrary.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.countP30
  }, 0)
  const countAll = departArrary.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.countAll
  }, 0)

  useEffect(() => {
    if (err) {
      Alert.alert(err)
    }
  }, [err]);

  const getCleanslate = (csuv_serial) => {
    const cleanslate = departArrary.find(el => el.csuv_serial === csuv_serial);
    dispatch(getCleanslateActions.getCleanslate(cleanslate))
      .then((res) => {
        props.navigation.navigate('CleanslateScreen');
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  }

  const refreshHMCleansate = (email, date) => {
    dispatch(getCleanslateActions.getHospiCleanslateCount(email, date))
    .then((res) => {
       dispatch(getCleanslateActions.getDepartCleanslates(res.group[name]))
         .then((res) => {
         })
    })
    .catch((error) => {
      setError('Error, please contact cleanslateUV')
      setError()
    })
  }

  const logout = async () => {
    await dispatch(authActions.signOut())
      .then((res) => {
        props.navigation.navigate('MainPage')
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  };

  const renderItem = ({ item }) => (
    <View style={{
      marginBottom: 10,
      marginTop: 10,
      marginLeft: 10,
      marginRight: 10,
      shadowColor: 'white',
      shadowOpacity: 1.8,
      shadowOffset: { width: 1, height: 2 },
      shadowRadius: 4,
      elevation: 5,
      borderRadius: 10,
      backgroundColor: 'white',
    }}>
      <ListItem
        title={
          <View>
            <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 15 }}>{item.location_name}</Text>
              </View>
              <TouchableHighlight
                style={{
                  backgroundColor: item.color,
                  borderRadius: 10,
                  elevation: 1,
                  width: 55,
                  height: 10,
                  marginTop: 10,
                  marginRight: 20,
                  marginLeft: 10
                }}
              >
                <Text></Text>
              </TouchableHighlight>
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 15 }}>{item.last_maint.substr(2, 8).replace(/-/g, '/')}</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 15, paddingLeft: 15 }}>{item.countAll}</Text>
              </View>
              <FontAwesomeIcon name="caret-right" size={25} color="grey" />
            </View>
          </View>
        }
        onPress={() => { getCleanslate(item.csuv_serial) }}
        containerStyle={{
          borderRadius: 10,
          backgroundColor: 'white',
        }}
      >
        chevron={{ color: 'black', size: 25 }}
      </ListItem>
    </View>
  )

  return (
    <View style={styles.container}>
       <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        style = {{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: 22
        }}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Description of status color bar: 
            {'\n'} red indicates mechanical error, 
            {'\n'} orange indicates bulb change time coming, green indicates normal</Text>
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#00B2EE" }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyle}>Get Started</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      <View>
        <Text style={{
          fontSize: 22, marginTop: 5, 
          marginLeft: 5, padding: 10, fontWeight: 'bold'
        }}>{departArrary[0].departmentName}</Text>
      </View>
      <View>
        <Text style={{
          fontSize: 22, fontWeight: 'bold', textAlign: 'right',
          color: '#00B2EE', marginRight: 20,
        }}>Sanitization Summary</Text>
      </View>
      <View>
        <Card containerStyle={{
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 20,
          width: '90%',
          backgroundColor: 'white',
          marginBottom: 10,
          marginTop: 10,
          maxWidth: 800,
          maxHeight: 180,
          borderRadius: 20,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5
        }}>
          <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'space-between',
            borderBottomWidth: 0.5,
            borderBottomColor: 'grey',
            marginBottom: 10
          }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginRight: 80 }}>
              Since Deployment</Text>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 20 }}>
              {countAll}</Text>
          </View>
          <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'space-between',
            borderBottomWidth: 0.5,
            borderBottomColor: 'grey',
            marginBottom: 10
          }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', }}>
              Past 30 Days</Text>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 20 }}>
              {countP30}</Text>
          </View>
          <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'space-between',
            borderBottomWidth: 0.5,
            borderBottomColor: 'grey',
            marginBottom: 5
          }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', }}>
              Past 7 Days</Text>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 20 }}>
              {countP7}</Text>
          </View>
          <View style={{
            flex: 1, flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', }}>
              Today</Text>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 20 }}>
              {countT}</Text>
          </View>
        </Card>
      </View>
      <View>
        <Text style={{
          fontSize: 22, fontWeight: 'bold', textAlign: 'right',
          color: '#00B2EE', marginRight: 20, marginTop: 2
        }}>Current Use</Text>
      </View>
      <View style={{ marginTop: 2, marginBottom: 5, marginLeft: 5 }}>
        <View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1, justifyContent: 'flex-start', paddingLeft: 10 }}>
              <Text style={{
                fontSize: 18, fontWeight: 'bold'
              }}>{'\n'}Location</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "column", marginRight: -15 }}>
              <View style={{ flex: 1, marginLeft: 35, paddingTop: 10 }}>
                <FontAwesomeIcon name="question-circle-o"
                  size={20} color='#00B2EE'
                  onPress={() => setModalVisible(!modalVisible)} />
              </View>
              <View style={{ flex: 1, }}>
                <Text style={{
                  fontSize: 18, fontWeight: 'bold', marginRight: 0, marginTop: -6
                }}>Status</Text>
              </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'flex-start', }}>
              <Text style={{
                fontSize: 18, fontWeight: 'bold',
              }}>Bulb Change</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'flex-start' }}>
              <Text style={{
                fontSize: 18, fontWeight: 'bold',
              }}>{'\n'}Use</Text>
            </View>
          </View>
        </View>
      </View>
      <FlatList
        keyExtractor={keyExtractor}
        data={departArrary}
        renderItem={renderItem}
        style={styles.flatList}
      />
      <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
      <Footer>
        <FooterTab style={{ backgroundColor: "#FFF" }}>
          <Button
            vertical
            onPress={() => { props.navigation.navigate('MainPage') }}>
            <FontAwesomeIcon name="home" size={25} color="grey" />
            <Text style={{
              fontSize:10,
              color: 'grey'
            }}>HOME</Text>
          </Button>
        </FooterTab>
        <FooterTab style={{ backgroundColor: "#FFF"}}>
        <Button
            vertical
            onPress={()=>{refreshHMCleansate(email, date)}}>
            <FontAwesomeIcon name="refresh" size={25} color="grey" />
            <Text style={{
              fontSize:10,
              color: 'grey'
            }}>REFRESH</Text>
          </Button>
        </FooterTab>
        <FooterTab style={{ backgroundColor: "white" }}>
        <Button
            vertical
            onPress={() => {logout() }}>
            <FontAwesomeIcon name="sign-out" size={25} color="grey" />
            <Text style={{
              fontSize:10,
              color: 'grey'
            }}>LOGOUT</Text>
          </Button>
        </FooterTab>
      </Footer>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    //backgroundColor: 'white',
    flexGrow: 0,
    marginBottom: 50
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#00B2EE",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontWeight: "600",
    fontSize: 20
  },
})

export default HMCleanslateScreen