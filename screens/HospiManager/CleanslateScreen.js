import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Alert,
  Modal,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import { Card } from "react-native-elements";
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Button, Footer, FooterTab, } from "native-base";
import { useDispatch, useSelector } from 'react-redux';
import * as getCleanslateActions from '../../store/actions/getCleanslates';
import * as authActions from '../../store/actions/auth';

const CleanslateScreen = props => {

  const dispatch = useDispatch();

  const cleanslate = useSelector(state => state.cleanslate.cleanslate);
  const [modalVisible, setModalVisible] = useState(false);
  const [secondModalVisible, setSecondModalVisible] = useState(false);
  const [thirdModalVisible, setThirdModalVisible] = useState(false);
  const [errors, setError] = useState();
  const status = cleanslate.color;
  const csuv_serial = cleanslate.csuv_serial;
  const email = cleanslate.email;
  const name = cleanslate.departmentName;
  const date = new Date();
  date.setHours(0, 0, 0, 0);

  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
  }, [errors]);

  const logout = async () => {
    await dispatch(authActions.signOut())
      .then((res) => {
        props.navigation.navigate('MainPage')
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  };

  const getCleanslate = (csuv_serial, email, date, name) => {
    dispatch(getCleanslateActions.getHospiCleanslateCount(email, date))
      .then((res1) => {
            const departCleanslateArray = res1.group[name];
            const cleanslate = departCleanslateArray.find(el => el.csuv_serial === csuv_serial);
            
            dispatch(getCleanslateActions.getCleanslate(cleanslate))
              .then((res) => {
              })
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  }

  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: 22
        }}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Description of status color bar:
            {'\n'} red indicates mechanical error,
            {'\n'} orange indicates bulb change time coming, green indicates normal</Text>
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#00B2EE" }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyle}>Get Started</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={secondModalVisible}
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: 22
        }}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Please Contact Cleanslate @ +1 (877) 553-6778 Ext 2</Text>
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#00B2EE" }}
              onPress={() => {
                setSecondModalVisible(!secondModalVisible);
              }}
            >
              <Text style={styles.textStyle}>Contact</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={thirdModalVisible}
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: 22
        }}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Please Contact CleanslateUV @ +1 (877) 553-6778 Ext 2 immediately</Text>
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#00B2EE" }}
              onPress={() => {
                setThirdModalVisible(!thirdModalVisible);
              }}>
              <Text style={styles.textStyle}>Contact</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      <View>
        <Text style={{
          fontSize: 22, marginTop: 5, marginBottom: 5,
          marginLeft: 5, padding: 5, fontWeight: 'bold'
        }}>{cleanslate.departmentName}</Text>
      </View>
      <View>
        <Text style={{
          fontSize: 20, fontWeight: 'bold', textAlign: 'right',
          color: 'black', marginRight: 20,
        }}>{cleanslate.location_name}</Text>
      </View>
      <View>
        <Text style={{
          fontSize: 20, fontWeight: 'bold', textAlign: 'right',
          color: '#00B2EE', marginRight: 20, marginTop: 5, marginBottom: 2
        }}>Location</Text>
      </View>
      <View>
        <Text style={{
          fontSize: 20, fontWeight: 'bold', textAlign: 'right',
          color: '#00B2EE', marginRight: 20, marginTop: 1, marginBottom: 10
        }}>Usage Analytics</Text>
      </View>
      <View>
        <View style={{ marginTop: 2, marginBottom: 10, marginLeft: 5, }}>
          <View>
            <View style={{ flex: 1, flexDirection: "row", marginBottom: 10 }}>
              <View style={{ flex: 1, flexDirection: "row", marginRight: -15, }}>
                <View style={{ flex: 1, }}>
                  <Text style={{
                    fontSize: 19, fontWeight: 'bold', marginLeft: 30
                  }}>Status</Text>
                </View>
                <View style={{ flex: 1, marginLeft: 0 }}>
                  <FontAwesomeIcon name="question-circle-o"
                    size={20} color='#00B2EE'
                    onPress={() => setModalVisible(!modalVisible)} />
                </View>
              </View>
              <View style={{ flex: 1, justifyContent: 'flex-start', }}>
                <Text style={{
                  fontSize: 19, fontWeight: 'bold',
                }}>Bulb Change</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{ marginTop: 2, marginBottom: 5, marginLeft: 5 }}>
          <View>
            <View style={{ flex: 1, flexDirection: "row", }}>
              <View style={{ flex: 1, flexDirection: "row", marginLeft: 20 }}>
                <TouchableHighlight
                  style={{
                    backgroundColor: status,
                    borderRadius: 10,
                    elevation: 1,
                    width: 55,
                    height: 10,
                    marginTop: 10,
                    marginRight: 20,
                    marginLeft: 10
                  }}
                >
                  <Text></Text>
                </TouchableHighlight>
              </View>
              <View style={{ flex: 1, justifyContent: 'flex-start', marginLeft: -30 }}>
                <Text style={{ fontSize: 18 }}>
                  {cleanslate.last_maint.substr(2, 8).replace(/-/g, '/')}</Text>
              </View>
            </View>
          </View>
        </View>
        <View>
          <Card containerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 20,
            width: '90%',
            backgroundColor: 'white',
            paddingHorizontal: 10,
            paddingVertical: 10,
            marginBottom: 10,
            marginTop: 25,
            maxWidth: 500,
            maxHeight: 180,
            borderRadius: 20,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5
          }}>
            <View style={{
              flex: 1, flexDirection: 'row',
              justifyContent: 'space-between',
              borderBottomWidth: 0.5,
              borderBottomColor: 'grey',
              marginBottom: 10,
            }}>
              <Text style={{ fontSize: 20, marginRight: 60, marginLeft: 10 }}>
                Since Deployment</Text>
              <Text style={{ fontSize: 20, paddingRight: 10 }}>
                {cleanslate.countAll}</Text>
            </View>
            <View style={{
              flex: 1, flexDirection: 'row',
              justifyContent: 'space-between',
              borderBottomWidth: 0.5,
              borderBottomColor: 'grey',
              marginBottom: 10
            }}>
              <Text style={{ fontSize: 20, marginRight: 80, marginLeft: 10  }}>
                Past 30 Days</Text>
              <Text style={{ fontSize: 20, paddingRight: 10 }}>
                {cleanslate.countP30}</Text>
            </View>
            <View style={{
              flex: 1, flexDirection: 'row',
              justifyContent: 'space-between',
              borderBottomWidth: 0.5,
              borderBottomColor: 'grey',
              marginBottom: 5
            }}>
              <Text style={{ fontSize: 20, marginRight: 80, marginLeft: 10  }}>
                Past 7 Days</Text>
              <Text style={{ fontSize: 20, paddingRight: 10 }}>
                {cleanslate.countP7}</Text>
            </View>
            <View style={{
              flex: 1, flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
              <Text style={{ fontSize: 20, marginRight: 80, marginLeft: 10 }}>
                Today</Text>
              <Text style={{ fontSize: 20, paddingRight: 10 }}>
                {cleanslate.countT}</Text>
            </View>
          </Card>
        </View>
        <View>
          {(status == 'orange') ?
            (<View><TouchableOpacity
              style={styles.LoginButton}
              onPress={() => setSecondModalVisible(!secondModalVisible)} >
              <View >
                <Text style={{
                  paddingTop: 5, textAlign: 'center', fontSize: 23,
                  color: 'white', fontWeight: 'bold',
                }}>Bulb Change</Text>
              </View>
            </TouchableOpacity></View>) : (<View></View>)}
        </View>
        <View>
          {(status == 'red') ?
            (<View><TouchableOpacity
              style={styles.LoginButton}
              onPress={() => setThirdModalVisible(!thirdModalVisible)} >
              <View >
                <Text style={{
                  paddingTop: 5, textAlign: 'center', fontSize: 22,
                  color: 'white', fontWeight: 'bold',
                }}>Contact CleanslateUV</Text>
              </View>
            </TouchableOpacity></View>) : (<View></View>)}
        </View>
      </View>
      <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
        <Footer>
          <FooterTab style={{ backgroundColor: "#FFF" }}>
            <Button
              vertical
              onPress={() => { props.navigation.navigate('HospitalManagerHome') }}>
              <FontAwesomeIcon name="home" size={25} color="grey" />
              <Text style={{
                fontSize: 10,
                color: 'grey'
              }}>HOME</Text>
            </Button>
          </FooterTab>
          <FooterTab style={{ backgroundColor: "#FFF" }}>
            <Button
              vertical
              onPress={() => { getCleanslate(csuv_serial, email, date, name) }}>
              <FontAwesomeIcon name="refresh" size={25} color="grey" />
              <Text style={{
                fontSize: 10,
                color: 'grey'
              }}>REFRESH</Text>
            </Button>
          </FooterTab>
          <FooterTab style={{ backgroundColor: "white" }}>
            <Button
              vertical
              onPress={() => { logout() }}>
              <FontAwesomeIcon name="sign-out" size={25} color="grey" />
              <Text style={{
                fontSize: 10,
                color: 'grey'
              }}>LOGOUT</Text>
            </Button>
          </FooterTab>
        </Footer>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatList: {
    flexGrow: 0,
    marginBottom: 50
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#00B2EE",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    padding:5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontWeight: "600",
    fontSize: 20
  },
  LoginButton: {
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10,
    paddingTop: 8,
    paddingBottom: 5,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#00B2EE',
    borderRadius: 50,
    elevation: 1,
    width: 300,
    height: 55,
  },
})

export default CleanslateScreen