import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Alert,
  Text,
  Modal,
  FlatList,
  TouchableHighlight,
  Image,
} from 'react-native';
import {  ListItem,  } from "react-native-elements";
import { useDispatch, useSelector } from 'react-redux';
import * as authActions from '../../store/actions/auth';
import * as getDeviceActions from '../../store/actions/getDevices';
import * as getCleanslateActions from '../../store/actions/getCleanslates';
import {
  Header, Button, Footer, FooterTab,
  Body, Container, Left, Right
} from "native-base";
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

const HospitalManagerScreen = props => {

  const [modalVisible, setModalVisible] = useState(false);
  const [errors, setError] = useState();
  const dispatch = useDispatch();
  const cleanslateArray = useSelector(state => state.getHospiCleanslateCount.group);
  const statusArray = useSelector(state => state.getHospiCleanslateCount.statusArray);
  let email = statusArray[0][4];
  let date = new Date();
  date.setHours(0, 0, 0, 0);
  const keyExtractor = (item, index) => index.toString();

  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
  }, [errors]);

  const getDepartCleanslates = (name) => {
    const departCleanslateArray = cleanslateArray[name];
    dispatch(getCleanslateActions.getDepartCleanslates(departCleanslateArray))
      .then((res) => {
        props.navigation.navigate('HMCleanslateScreen')
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  }

  const refreshHospitalManager = (email, date) => {
    dispatch(getCleanslateActions.getHospiCleanslateCount(email, date))
    .then((res) => {
      dispatch(getDeviceActions.getHospiDeviceCount(email, date))
        .then((res1) => {
          props.navigation.navigate('HospitalManagerHome');
        })
    })
    .catch((error) => {
      setError('Error, please contact cleanslateUV')
      setError()
    })
  }

  const logout = async () => {
    await dispatch(authActions.signOut())
      .then((res) => {
        props.navigation.navigate('MainPage')
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  };

  const renderItem = ({ item }) => (
    <View style={{
      marginBottom: 5,
      marginTop: 5,
      marginLeft: 15,
      marginRight: 15,
      shadowColor: 'white',
      shadowOpacity: 1.8,
      shadowOffset: { width: 1, height: 2 },
      shadowRadius: 4,
      elevation: 5,
      borderRadius: 10,
    }}>
      <ListItem
        title={
          <View>
            <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 20 }}>{item[0]}</Text>
              </View>
              <TouchableHighlight
                style={{
                  backgroundColor: item[2],
                  borderRadius: 10,
                  elevation: 1,
                  width: 55,
                  height: 10,
                  marginTop: 10,
                  marginRight: 30,
                }}
              >
                <Text></Text>
              </TouchableHighlight>
              <FontAwesomeIcon name="caret-right" size={25} color="grey" />
            </View>
          </View>
        }
        onPress={() => { getDepartCleanslates(item[0]) }}
        containerStyle={{
          borderRadius: 10,
          backgroundColor: 'white',
        }}
      >
        chevron={{ color: 'black', size: 25 }}
      </ListItem>
    </View>
  )

  return (
    <Container style={{ backgroundColor: 'transparent' }} >
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginTop: 22

        }}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Description of status color bar:
            {'\n'} red indicates mechanical error,
            {'\n'} orange indicates bulb change time coming, green indicates normal</Text>
            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#00B2EE" }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <Text style={styles.textStyle}>Get Started</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
      <View style={{ backgroundColor: 'transparent' }}>
        <Text style={{
          fontSize: 32, marginTop: 10, marginBottom: 5,
          marginLeft: 5, padding: 10, fontWeight: 'bold'
        }}>{statusArray[0][3]}</Text>
      </View>
      <View style={{ marginTop: 10, marginBottom: 10, marginLeft: 5 }}>
        <View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1, justifyContent: 'flex-start', marginLeft: 15, marginRight: 70 }}>
              <Text style={{
                fontSize: 22,
              }}>Department</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'flex-start', flexDirection: "row" }}>
              <Text style={{
                fontSize: 22, marginRight: 5
              }}>Status</Text>
              <View style={{ paddingTop: 2 }}>
                <FontAwesomeIcon name="question-circle-o"
                  size={25} color='#00B2EE'
                  onPress={() => setModalVisible(!modalVisible)} />
              </View>
            </View>
          </View>
        </View>
      </View>
      <FlatList
        keyExtractor={keyExtractor}
        data={statusArray}
        renderItem={renderItem}
      />
      <Footer>
        <FooterTab style={{ backgroundColor: "#FFF" }}>
          <Button
            vertical
            onPress={() => { props.navigation.navigate('MainPage') }}>
            <FontAwesomeIcon name="home" size={25} color="grey" />
            <Text style={{
              fontSize: 10,
              color: 'grey'
            }}>HOME</Text>
          </Button>
        </FooterTab>
        <FooterTab style={{ backgroundColor: "#FFF" }}>
          <Button
            vertical
            onPress={() => { refreshHospitalManager(email, date) }}>
            <FontAwesomeIcon name="refresh" size={25} color="grey" />
            <Text style={{
              fontSize: 10,
              color: 'grey'
            }}>REFRESH</Text>
          </Button>
        </FooterTab>
        <FooterTab style={{ backgroundColor: "white" }}>
          <Button
            vertical
            onPress={() => { logout() }}>
            <FontAwesomeIcon name="sign-out" size={25} color="grey" />
            <Text style={{
              fontSize: 10,
              color: 'grey'
            }}>LOGOUT</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  )
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  flatList: {
    flex: 1,
    flexGrow: 0
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#00B2EE",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontWeight: "600",
    fontSize: 20
  },
})

export default HospitalManagerScreen