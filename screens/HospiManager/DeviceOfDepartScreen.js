import React from 'react'
import {
  View,
  StyleSheet,
  Button,
  Text,
} from 'react-native';
import { useSelector } from 'react-redux';
import { Icon } from 'react-native-elements';

const DeviceOfDepartScreen = prop => {

  const device = useSelector(state => state.getDeviceOfDepart.device);

  return (
    <View style={styles.container}>
      <Container>
        <Content>
          <View style={{ height: 70, backgroundColor: 'white', borderBottomWidth: 1, paddingTop: 20 }} >
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1, paddingLeft: 10 }}>
                <Text style={{ fontSize: 22, fontWeight: 'bold' }}>
                  {device.DepartmentName}</Text>
              </View>
              <View
                style={{ flex: 1, paddingRight: 5 }}>
                <Text style={{ fontSize: 18, fontWeight: '200', textAlign: 'center', paddingTop: 5, paddingLeft: 50 }}>
                  Your Device(s)</Text>
              </View>
            </View>
          </View>
          <View style={{ height: 50, backgroundColor: 'white', borderBottomWidth: 1, paddingTop: 15 }}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ justifyContent: 'flex-start', paddingLeft: 10 }}>Tagged Device(s)</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={{ justifyContent: 'flex-end', }}>Device(s) name</Text>
              </View>
            </View>
          </View>
          <View style={{ height: 50, backgroundColor: 'white', borderBottomWidth: 1, paddingTop: 15 }}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ justifyContent: 'flex-start', paddingLeft: 10 }}> {device.deviceType}</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={{ justifyContent: 'flex-end', fontWeight: 'bold' }}> {device.deviceName}</Text>
              </View>
            </View>
          </View>
          <View style={{ height: 60, backgroundColor: 'white', borderTopWidth: 1, paddingTop: 15 }}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1, paddingLeft: 10 }}>
                <Text style={{ fontSize: 16, fontWeight: '100' }}>
                  Total Devices Sanitized {'\n'} (Since Deployment)
              </Text>
              </View>
              <View
                style={{ flex: 1, paddingRight: 10 }}>
                <Text style={{ fontSize: 18, fontWeight: '100', textAlign: 'right' }}>
                  {device.countAll}</Text>
              </View>
            </View>
          </View>
          <View style={{ height: 60, backgroundColor: 'white', paddingTop: 15 }}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1, paddingLeft: 10 }}>
                <Text style={{ fontSize: 16, fontWeight: '100' }}>
                  Devices Sanitized {'\n'} (Past 30 days)</Text>
              </View>
              <View
                style={{ flex: 1, paddingRight: 10 }}>
                <Text style={{ fontSize: 18, fontWeight: '100', textAlign: 'right' }}>
                  {device.countP30}</Text>
              </View>
            </View>
          </View>
          <View style={{ height: 60, backgroundColor: 'white', paddingTop: 15 }}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1, paddingLeft: 10 }}>
                <Text style={{ fontSize: 16, fontWeight: '100' }}>
                  Devices Sanitized {'\n'} (Past Week)</Text>
              </View>
              <View
                style={{ flex: 1, paddingRight: 10 }}>
                <Text style={{ fontSize: 18, fontWeight: '100', textAlign: 'right' }}>
                  {device.countP7}</Text>
              </View>
            </View>
          </View>
          <View style={{ height: 60, backgroundColor: 'white', paddingTop: 15 }}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flex: 1, paddingLeft: 10 }}>
                <Text style={{ fontSize: 16, fontWeight: '100' }}>
                  Devices Sanitized {'\n'} (Today)</Text>
              </View>
              <View
                style={{ flex: 1, paddingRight: 10 }}>
                <Text style={{ fontSize: 18, fontWeight: '100', textAlign: 'right' }}>
                  {device.countT}</Text>
              </View>
            </View>
          </View>
        </Content>
        <Footer>
          <FooterTab style={{ backgroundColor: "#FFF" }}>
            <Button
              vertical
              onPress={() => props.navigation.navigate("MainPage")}>
              <Icon name="home" />
              <Text>Home</Text>
            </Button>
            <Button
              vertical
              onPress={() => props.navigation.navigate("HospitalManagerHome")}>
              <Icon name="refresh" />
              <Text>Refresh</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
})

export default DeviceOfDepartScreen;