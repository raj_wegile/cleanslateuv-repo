import React, {useState, useEffect} from "react";
import {
  View,
  Alert,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import * as deviceUserActions from '../../store/actions/userDevice'
import * as authActions from '../../store/actions/auth'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Tooltip, Card } from 'react-native-elements';
import { Content, Button, Footer, FooterTab, Container } from "native-base";

const logo = require('../../images/uvlogo2.png');

const DeviceConfirmScreen = props => {
  
  const [errors, setError] = useState( )
  const dispatch = useDispatch();
  const deviceName = useSelector(state => state.getDevice.deviceName);
  const Tag_Number = useSelector(state => state.getDevice.Tag_Number);
  const user = useSelector(state => state.auth.user);
  const usage = useSelector(state => state.getDevice.usage);
  const device_id = useSelector(state => state.getDevice.device_id);
  const date = new Date();
  date.setHours(0, 0, 0, 0)
  
  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
  }, [errors]);

  const deviceCount = () => {
    dispatch(deviceUserActions.getDeviceCount(date, device_id))
      .then((res) => {
        props.navigation.navigate('DeviceDetail')
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  }

  const getDevice = () => {
    dispatch(deviceUserActions.getDevice(user.username.toUpperCase()))
      .then((res) => {
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  }

  const logout = async () => {
    await dispatch(authActions.signOut())
      .then((res) => {
        props.navigation.navigate('MainPage')
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  };

  DeviceConfirmScreen.navigationOptions = {
    headerTitle: () => <Image source={logo} />,
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: "white"
    },
    headerTitleStyle: {
      fontWeight: "bold",
    },
    headerTitleAlign: 'center',
    headerLeft: () => (
      <View style={{ paddingLeft: 10 }}></View>
    ),
  };

  return (
    <Container>
      <Content>
        <View style={styles.screen}>
          <View>
            <Text style={{
              fontSize: 28, fontWeight: 'bold', textAlign: 'center',
              color: 'black', marginTop: 10
            }}>Please confirm your {'\n'} device details </Text>
          </View>
          <View >
            <Card containerStyle={{
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              backgroundColor: 'white',
              marginBottom: 10,
              marginTop: 20,
              maxWidth: 900,
              maxHeight: 200,
              borderRadius: 20,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5
            }}>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomWidth: 0.5,
                borderBottomColor: 'grey',
                marginBottom: 10
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginRight: 80 }}>
                  4 Digit Code</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {Tag_Number}</Text>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between', borderBottomWidth: 0.5,
                borderBottomColor: 'grey', marginBottom: 10
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', }}>
                  Mobile Device</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {deviceName}</Text>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', }}>
                  Sanitizations {'\n'} per day</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {usage}</Text>
              </View>
            </Card>
          </View>
          <View style={{ alignSelf: 'center', marginTop: 10 }}>
            <TouchableOpacity
              style={styles.LoginButton} onPress={deviceCount}>
              <View >
                <Text style={{ fontSize: 20, textAlign: 'center', color: '#fff' }}>Confirm</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View>
          </View>
        </View>
      </Content>
      <Footer>
        <FooterTab style={{ backgroundColor: "#FFF", borderRightColor: '#c9ecf3', borderRightWidth: 1 }}>
          <Button
            vertical
            onPress={() => { getDevice() }}>
            <FontAwesomeIcon name="refresh" size={26} color="grey" />
            <Text style={{
              fontWeight: 'bold',
              color: 'grey',
              fontSize: 13
            }}>refresh</Text>
          </Button>
        </FooterTab>
        <FooterTab style={{ backgroundColor: "white" }}>
          <Button
            vertical
            onPress={() => { logout() }}>
            <FontAwesomeIcon name="sign-out" size={30} color="grey" />
            <Text style={{
              fontWeight: 'bold',
              color: 'grey',
              fontSize: 13
            }}>logout</Text>
          </Button>
        </FooterTab>
      </Footer>
      <View style={{ position: 'absolute', bottom: 60, alignSelf: 'center' }}>
        <Tooltip backgroundColor='white' width={250}
          height={40} popover={<Text style={{ fontSize: 18 }}>Contact cleanslateUV {"\n"} @ +1(877)553-6778 Ext 2</Text>}>
          <Text style={{ fontSize: 22, color: 'black', fontWeight: 'bold' }} >Need Help?</Text>
        </Tooltip>
      </View>
    </Container>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'white',
    width: '100%',
  },
  authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    marginBottom: 10,
    maxWidth: 280,
    maxHeight: 300,
    borderRadius: 20
  },
  buttonContainer: {
    width: '100%',
    marginTop: 10,
    alignItems: 'center',
  },
  LoginButton: {
    justifyContent: 'center',
    marginLeft: 0,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#00B2EE',
    borderRadius: 50,
    elevation: 1,
    width: 300,
    height: 60,
  },
});

export default DeviceConfirmScreen;