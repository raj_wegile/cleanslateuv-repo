import React, { useEffect, useState } from 'react'
import {
  View,
  Alert,
  StyleSheet,
  Image,
  Text,
} from 'react-native';
import { Card } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import * as deviceUserActions from '../../store/actions/userDevice';
import * as authActions from '../../store/actions/auth';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Content, Footer, FooterTab, Container, Button } from "native-base";

const logo = require('../../images/uvlogo2.png')

const DeviceDetailsScreen = props => {

  const dispatch = useDispatch();
  const [errors, setError] = useState();
  const countAll = useSelector(state => state.getDeviceCount.countAll);
  const countToday = useSelector(state => state.getDeviceCount.countToday);
  const countP7 = useSelector(state => state.getDeviceCount.countP7);
  const countP30 = useSelector(state => state.getDeviceCount.countP30);
  const deviceName = useSelector(state => state.getDevice.deviceName);
  const Tag_Number = useSelector(state => state.getDevice.Tag_Number);
  const usage = useSelector(state => state.getDevice.usage);
  const device_id = useSelector(state => state.getDevice.device_id);
  const date = new Date();
  date.setHours(0, 0, 0, 0)

  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
  }, [errors]);

  const refreshDeviceCount = () => {
    dispatch(deviceUserActions.getDeviceCount(date, device_id))
      .then((res) => {
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  }

  const logout = async () => {
    await dispatch(authActions.signOut())
      .then((res) => {
        props.navigation.navigate('MainPage')
      })
      .catch((error) => {
        setError('Error, please contact cleanslateUV')
        setError()
      })
  };

  DeviceDetailsScreen.navigationOptions = {
    headerTitle: () => <Image source={logo} />,
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: "white"
    },
    headerTitleStyle: {
      fontWeight: "bold",
    },
    headerTitleAlign: 'center',
    headerLeft: () => (
      <View style={{ paddingLeft: 10 }}></View>
    ),
  };

  return (
    <Container>
      <Content>
        <View style={styles.screen}>
          <View>
            <Text style={{
              fontSize: 20, fontWeight: 'bold', textAlign: 'left',
              color: 'black', marginTop: 10, marginLeft: 20
            }}>You are required to {'\n'}sanitize your device {'\n'}at least {usage} time(s) a day </Text>
          </View>
          <View>
            <Card containerStyle={{
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: 38,
              width: '80%',
              backgroundColor: 'white',
              marginBottom: 10,
              marginTop: 20,
              maxWidth: 800,
              maxHeight: 120,
              borderRadius: 20,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5
            }}>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomWidth: 0.5,
                borderBottomColor: 'grey',
                marginBottom: 10
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginRight: 80 }}>
                  4 Digit Code</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {Tag_Number}</Text>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between'
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', }}>
                  Mobile Device</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {deviceName}</Text>
              </View>
            </Card>
          </View>
          <View>
            <Text style={{
              fontSize: 22, fontWeight: 'bold', textAlign: 'right',
              color: '#00B2EE', marginTop: 20, marginRight: 20,
            }}>Sanitization Summary</Text>
          </View>
          <View>
            <Card containerStyle={{
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: 20,
              width: '90%',
              backgroundColor: 'white',
              marginBottom: 10,
              marginTop: 40,
              maxWidth: 900,
              maxHeight: 220,
              borderRadius: 20,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5
            }}>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomWidth: 0.5,
                borderBottomColor: 'grey',
                marginBottom: 10
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginRight: 80 }}>
                  Since Deployment</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {countAll}</Text>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomWidth: 0.5,
                borderBottomColor: 'grey',
                marginBottom: 10
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', }}>
                  Past 30 Days</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {countP30}</Text>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomWidth: 0.5,
                borderBottomColor: 'grey',
                marginBottom: 5
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', }}>
                  Past 7 Days</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {countP7}</Text>
              </View>
              <View style={{
                flex: 1, flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', }}>
                  Today</Text>
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 20 }}>
                  {countToday}</Text>
              </View>
            </Card>
          </View>
        </View>
      </Content>
      <Footer>
        <FooterTab style={{ backgroundColor: "#FFF", borderRightColor: '#c9ecf3', borderRightWidth: 2 }}>
          <Button
            vertical
            onPress={() => { refreshDeviceCount() }}>
            <FontAwesomeIcon name="refresh" size={26} color="grey" />
            <Text style={{
              fontWeight: 'bold',
              color: 'grey',
              fontSize: 13
            }}>refresh</Text>
          </Button>
        </FooterTab>
        <FooterTab style={{ backgroundColor: "white" }}>
          <Button
            vertical
            onPress={() => { logout() }}>
            <FontAwesomeIcon name="sign-out" size={28} color="grey" />
            <Text style={{
              fontWeight: 'bold',
              color: 'grey',
              fontSize: 13
            }}>logout</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    width: '100%',
  },
  authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    marginBottom: 10,
    maxWidth: 280,
    maxHeight: 300,
    borderRadius: 20
  },
  buttonContainer: {
    width: '100%',
    marginTop: 10,
    alignItems: 'center',
  },
  LoginButton: {
    justifyContent: 'center',
    marginLeft: 0,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#00B2EE',
    borderRadius: 50,
    elevation: 1,
    width: 300,
    height: 60,
  },
});

export default DeviceDetailsScreen;