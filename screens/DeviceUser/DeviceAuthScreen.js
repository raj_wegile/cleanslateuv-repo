import React, { useReducer, useCallback, useState, useEffect } from 'react';
import {
  ScrollView,
  View,
  ImageBackground,
  KeyboardAvoidingView,
  StyleSheet,
  Image,
  Alert,
  ActivityIndicator,
  Text,
  TouchableOpacity,
} from 'react-native';
import Input from '../../components/Input';
import { useDispatch } from 'react-redux';
import * as deviceUserActions from '../../store/actions/userDevice'
import * as authActions from '../../store/actions/auth'
import { Tooltip } from 'react-native-elements';

const logo = require('../../images/uvlogo2.png');
const background = require('../../images/linesbackground2.png');
const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    };
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    };
    let updatedFormIsValid = false;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    };
  }
  return state;
};

const DeviceAuthScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [errors, setError] = useState( )
  const dispatch = useDispatch();
  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      username: 'null',
      password: 'null',
    },
    inputValidities: {
      username: false,
      password: false,
    },
    formIsValid: false
  });

  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
  }, [errors]);

  const signinHandler = async () => {
    let user;
    setIsLoading(true);
    await dispatch(authActions.signIn(formState.inputValues.username, formState.inputValues.password))
      .then((res) => {
        if (res.user.challengeName === 'NEW_PASSWORD_REQUIRED') { props.navigation.navigate('AuthConfirm') }
        else {
          user = res.user.username.toUpperCase()
          dispatch(deviceUserActions.getDevice(user))
            .then((res) => {
              props.navigation.navigate('DeviceInfo')
            })
        }
      })
      .catch((error) => {
        setError(error.message)
        setError()
        setIsLoading(false)
      })
  }

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      })
    },
    [dispatchFormState]
  );

  DeviceAuthScreen.navigationOptions = {
    headerTitle: () => <Image source={logo} />,
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: "white"
    },
    headerTitleStyle: {
      fontWeight: "bold",
    },
    headerTitleAlign: 'center',
    headerLeft: () => (
      <View style={{ paddingLeft: 10 }}></View>
    ),
  };
  
  return (
    <ImageBackground
      source={background}
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        width: '100%',
        backgroundColor: 'white'
      }}>
      <KeyboardAvoidingView
        behavior="padding"
        keyboardVerticalOffset={50}
        style={styles.screen}
      >
        <View>
          <Text style={{
            fontSize: 32, fontWeight: 'bold', textAlign: 'center',
            color: 'black', marginTop: 80
          }}>Login</Text>
        </View>
        <View style={styles.authContainer}>
          <ScrollView style={{ width: '100%' }}>
            <Input
              id="username"
              label="TagNumber"
              keyboardType="visible-password"
              required
              autoCapitalize="none"
              errorText="Please enter a valid username."
              onInputChange={inputChangeHandler}
              initialValue=""
            />
            <Input
              id="password"
              label="Password"
              keyboardType="default"
              secureTextEntry
              required
              minLength={5}
              autoCapitalize="none"
              errorText="Please enter a valid password."
              onInputChange={inputChangeHandler}
              initialValue=""
            />
          </ScrollView>
        </View>
        <View style={{ alignSelf: 'center' }}>
          {isLoading ? <ActivityIndicator style={{ paddingVertical: 30 }} /> :
            <TouchableOpacity
              style={styles.LoginButton} onPress={signinHandler}>
              <View >
                <Text style={{ fontSize: 20, textAlign: 'center', color: '#fff' }}>LOGIN</Text>
              </View>
            </TouchableOpacity>}
        </View>
        <View>
        </View>
      </KeyboardAvoidingView>
      <View style={{ position: 'absolute', bottom: 60 }}>
        <Tooltip backgroundColor='white' width={250}
          height={40} popover={<Text style={{ fontSize: 18 }}>Contact cleanslateUV {"\n"} @ +1(877)553-6778 Ext 2</Text>}>
          <Text style={{ fontSize: 22, color: 'black', fontWeight: 'bold' }} >Need Help?</Text>
        </Tooltip>
      </View>
    </ImageBackground>
  )
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'white',
    width: '100%',
  },
  authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    marginBottom: 10,
    maxWidth: 280,
    maxHeight: 300,
    padding: 5
  },
  buttonContainer: {
    width: '100%',
    marginTop: 10,
    alignItems: 'center',
  },
  LoginButton: {
    justifyContent: 'center',
    marginLeft: 0,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#00B2EE',
    borderRadius: 50,
    elevation: 1,
    width: 300,
    height: 60,
  },
});

export default DeviceAuthScreen;

