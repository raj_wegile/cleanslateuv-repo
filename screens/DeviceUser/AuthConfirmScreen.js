import React, { useReducer, useCallback, useState, useEffect } from 'react';
import {
  ScrollView,
  View,
  KeyboardAvoidingView,
  StyleSheet,
  Image,
  Alert,
  Text,
  TouchableHighlight
} from 'react-native';
import Input from '../../components/Input';
import { useDispatch, useSelector } from 'react-redux';
import * as authActions from '../../store/actions/auth';
import formReducer from '../../store/reducers/auth/form';

const logo = require('../../images/uvlogo2.png')
const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const  AuthConfirmScreen = props => {
  const [errors, setError] = useState()
  const user = useSelector(state => state.auth.user);
  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      password: 'null'
    },
    inputValidities: {
      password: false,
    },
    formIsValid: false
  });
  const dispatch = useDispatch();
  
  useEffect(()=> {
    if(errors){
      Alert.alert(errors)
    }
  }, [errors]);

  const newPasswordHandler = () => {
      dispatch(authActions.changePassword(user, formState.inputValues.password))
        .then((res) => {
             props.navigation.navigate('MainPage')
        })
        .catch((error) => {
          setError('Error, please contack cleansateUV')
          setError( )
        })
    }

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      });
    },
    [dispatchFormState]
  );


  AuthConfirmScreen.navigationOptions = {
    headerTitle: () => <Image source={logo} />,
    headerTintColor: "black",
    headerStyle: {
      backgroundColor: "white"
    },
    headerTitleStyle: {
      fontWeight: "bold",
    },
    headerTitleAlign: 'center',
    headerLeft: () => (
      <View style={{ paddingLeft: 10 }}></View>
    ),
  };

  return (
    <KeyboardAvoidingView
      behavior="padding"
      keyboardVerticalOffset={50}
      style={styles.screen}
    >
      <View>
          <Text>authconfirm</Text>
      </View >
      <View style={styles.authContainer}>
        <ScrollView style={{ width: '60%' }}>
        <Input
              id="password"
              label="password"
              keyboardType="default"
              secureTextEntry
              required
              minLength={5}
              autoCapitalize="none"
              errorText="Please enter a valid password."
              onInputChange={inputChangeHandler}
              initialValue=""
            />
          <View>
          <TouchableHighlight underlayColor= '#0286c2' 
          style={styles.customBtnBG} onPress={newPasswordHandler}>
          <View >
            <Text style={{ fontSize: 20, textAlign: 'center', color: '#fff' }}>CONFIRM</Text>
          </View>
        </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  )
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingBottom:100
  },
  authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
    maxWidth: 400,
    maxHeight: 400,
    padding: 10
  },
  buttonContainer: {
    width: '80%',
    marginTop: 10,
    alignItems: 'center',
  },
  customBtnBG: {
    marginRight: 40,
    marginLeft: 40,
    marginTop: 20,
    marginBottom: 20,
    paddingTop: 10,
    paddingBottom: 15,
    backgroundColor: '#03A9F4',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    width: 100,
    height: '45%'
    }
});


export default AuthConfirmScreen;
