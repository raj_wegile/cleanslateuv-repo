import React, { useReducer, useCallback, useState, useEffect } from 'react';
import {
  View,
  BackHandler,
  StyleSheet,
  Linking,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  Text,
  TouchableHighlight,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';
import { Tooltip } from 'react-native-elements';
import Input from '../components/Input';
import { useDispatch } from 'react-redux';
import * as authActions from '../store/actions/auth';
import * as getDeviceActions from '../store/actions/getDevices';
import * as getCleanslateActions from '../store/actions/getCleanslates';
import * as getUserActions from '../store/actions/getUsers';
import formReducer from '../store/reducers/auth/form';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

// The main container used to manage the login for the user
const managerAuthScreen = props => {

  // Assigning constants for dispatching user information
  // Collecting state information about the form and error documentation
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isShown, setIsShown] = useState(false);
  const [errors, setError] = useState();

  // Handles the Android hardware back button press
  useEffect(() => {
    if (errors) {
      Alert.alert(errors)
    }
    Keyboard.addListener('keyboardDidShow', keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', keyboardDidHide);
    BackHandler.addEventListener('hardwareBackPress', onBackPress)
    return () => {
      Keyboard.removeListener('keyboardDidShow', keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', keyboardDidHide);
      BackHandler.removeEventListener('hardwareBackPress', onBackPress)
    }
  }, [errors])

  // Ensures that back press is only blocked when the screen is focused
  const onBackPress = () => {
    if (props.navigation.isFocused())
      return true;
    return false;
  }

  // Ensures that the keyboard visibility is handled to remove the 'Need Help' button
  const keyboardDidShow = () => setIsShown(true);
  const keyboardDidHide = () => setIsShown(false);

  // State variables to hold the data present in the form
  // at time of login or authentication
  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      username: ' ',
      password: ' ',
    },
    inputValidities: {
      username: false,
      password: false,
    },
    formIsValid: false
  });

  const date = new Date();
  date.setHours(0, 0, 0, 0)

  // This constant is used to handle the user's login
  // Pulls data from the form to sign the user in with the associated fields
  const signinHandler = () => {
    setIsLoading(true);
    // The authentication from Amplify to send the sign-in request
    dispatch(authActions.signIn(formState.inputValues.username, formState.inputValues.password))
      .then((res) => {
        const email = res.user.attributes.email
        // Checking if the user is a department manager
        if (res.user.signInUserSession.accessToken.payload['cognito:groups'][0] == "departmentManager") {
          // Gathering the rest of the user's data, including the array of cleanslates available to the user,
          // and finally sending the login information to update their login count
          dispatch(getDeviceActions.getDepartDeviceCount(email, date))
            .then((res) =>
              dispatch(getCleanslateActions.getDepartCleanslateCount(email, date))
                .then((res1) =>
                  dispatch(getUserActions.sendUserLogin(email))
                    .then((res2) =>
                      props.navigation.navigate('DepartManagerHome')
                    )
                )
            )
            .catch((error) => {
              setError("Error, please contact CleanslatUV")
              setError()
              setIsLoading(false);
            })
        } else {
          // For the Hospital Managers instead of the Department Managers
          // Gathering the rest of the user's data, including the array of cleanslates available to the user,
          // and finally sending the login information to update their login count
          dispatch(getCleanslateActions.getHospiCleanslateCount(email, date))
            .then((res) =>
              dispatch(getDeviceActions.getHospiDeviceCount(email, date))
                .then((res1) =>
                  dispatch(getUserActions.sendUserLogin(email))
                    .then((res2) =>
                      props.navigation.navigate('HospitalManagerHome')
                    )
                )
            )
            .catch((error) => {
              setError("Error, please contact CleanslatUV")
              setError()
              setIsLoading(false);
            })
        }
      })
      .catch((error) => {
        if (error.message.includes('radix'))
          setError('Incorrect username or password.')
        else
          setError(error.message)
        setError()
        setIsLoading(false)
      })
  }

  // This constant handles the navigation to the forgot password screen
  const forgotPasswordHandler = () => {
    props.navigation.navigate('ForgotPassword')
  }

  // This constant handles the form input to determine if the user has entered
  // valid information to be parsed by the reader
  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier
      })
    },
    [dispatchFormState]
  );

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        width: '100%',
        backgroundColor: 'white',
      }}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? "padding" : "height"}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}
          style={styles.screen}>
          <View>
            <Text style={{
              fontSize: 32, textAlign: 'center',
              color: '#003459', marginTop: 40, fontFamily: 'euclid-triangle-semibold'
            }}>Login</Text>
          </View>
          <View style={styles.authContainer}>
              <Input
                id="username"
                label="Username"
                keyboardType="default"
                required
                autoCapitalize="none"
                errorText="Please enter a valid username."
                onInputChange={inputChangeHandler}
                initialValue=""
              />
              <Input
                id="password"
                label="Password"
                keyboardType="default"
                secureTextEntry
                required
                autoCapitalize="none"
                errorText="Please enter a valid password."
                onInputChange={inputChangeHandler}
                initialValue=""
              />
          </View>
          <TouchableHighlight underlayColor='#f5f5f5'
            onPress={forgotPasswordHandler}>
            <View >
              <Text style={{
                paddingTop: 5, paddingLeft: 120, fontSize: 15, fontWeight: '100', marginBottom: 30,
                textAlign: 'center', color: 'grey', fontFamily: 'euclid-triangle'
              }}>Forgot Password?</Text>
            </View>
          </TouchableHighlight>
          <View style={{ alignSelf: 'center' }}>
            {isLoading ? <ActivityIndicator style={{ paddingVertical: 30 }} /> :
              <TouchableOpacity
                style={styles.LoginButton} onPressIn={() => Keyboard.dismiss()} onPressOut={signinHandler}>
                <View style={{ alignSelf: 'center' }}>
                  <Text style={{ fontSize: 20, textAlign: 'center', color: '#fff', fontFamily: 'euclid-triangle' }}>LOGIN</Text>
                </View>
              </TouchableOpacity>}
          </View>
          <View style={{ position: 'absolute', bottom: 0.08*Dimensions.get('window').height }}>
            {isShown && Platform.OS === 'android' ? <View/> :
            <Tooltip backgroundColor='#ECF3F7' width={Dimensions.get('window').width/1.33}
              height={80} popover={
                <View>
                  <Text style={{ fontSize: 16, fontFamily: 'euclid-triangle-semibold', color: '#003459' }}>Contact CleanSlate UV</Text>
                  <Text onPress={() => { Linking.openURL('tel:+1 (877) 553-6778 2') }} style={{ fontSize: 16, paddingTop: 10, fontFamily: 'euclid-triangle' }}>@ +1 (877) 553-6778 Ext 2</Text>
                  <Text onPress={() => { Linking.openURL('mailto:engineering@cleanslateuv.com') }} style={{ fontSize: 16, fontFamily: 'euclid-triangle' }}>@ engineering@cleanslateuv.com</Text>
                </View>
                }>
              <Text style={{ fontSize: 22, color: '#003459', fontFamily: 'euclid-triangle-semibold' }}>Need Help?</Text>
            </Tooltip>}
          </View>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </View>
  )
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  authContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    maxWidth: 280,
    maxHeight: 300,
    padding: 5
  },
  buttonContainer: {
    width: '100%',
    marginTop: 10,
    alignItems: 'center',
  },
  LoginButton: {
    justifyContent: 'center',
    marginLeft: 0,
    marginTop: 20,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#00ADEE',
    borderRadius: 50,
    elevation: 1,
    width: 300,
    height: 60,
  },
  PopoverButton: {
    flex: 1,
    flexDirection: "row",
    alignSelf: 'center',
    marginRight: 15,
    marginLeft: 30,
    marginTop: 10,
    marginBottom: 20,
    paddingTop: 8,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 0,
    backgroundColor: 'white',
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    width: 50,
    height: 50,
  }
});


export default managerAuthScreen;