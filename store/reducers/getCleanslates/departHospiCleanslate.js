
import {GETDEPARTMENTCLEANSLATESUCCEEDED} from '../../actions/getCleanslates';

const initialState = {
  departArray: null,
  status: null,
  color: null,
  isFetching: false
}

const departHospiCleanslate = (state = initialState, action) => {
    switch (action.type) {
     case GETDEPARTMENTCLEANSLATESUCCEEDED:
          return {
            departArray: action.departArray,
            color: action.color,
            status: action.status,
            isFetching: true
          }
      default:
        return state;
    }
  };
  
  export default departHospiCleanslate