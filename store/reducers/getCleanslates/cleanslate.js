import { GETCLEANSLATE } from '../../actions/getCleanslates';

const initialState = {
  cleanslate:null,
  error:null,
  warning:null,
  isFetching: false
}

const cleanslate = (state = initialState, action) => {
    switch (action.type) {
      case GETCLEANSLATE:
          return {
            cleanslate: action.cleanslate,
            error: action.error,
            warning: action.warning,
            isFetching: true,
          };
      default:
        return state;
    }
  };
  
  export default cleanslate