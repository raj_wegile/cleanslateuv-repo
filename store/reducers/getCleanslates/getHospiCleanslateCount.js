import { GETHOSPITALCLEANSLATESUCCEEDED, GETDEPARTCLEANSLATES } from '../../actions/getCleanslates';

const initialState = {
  group:null,
  departGroup:null,
  departNameArray:null,
  hospiName:null,
  statusArray: null,
  isFetching: false
}

const getHospiCleanslateCount = (state = initialState, action) => {
    switch (action.type) {
      case GETHOSPITALCLEANSLATESUCCEEDED:
        return {
           group:action.group,
           departNameArray:action.departNameArray,
           hospiName: action.hospiName,
           statusArray:action.statusArray,
           isFetching: true,
        };
      default:
        return state;
    }
  };
  
  export default getHospiCleanslateCount