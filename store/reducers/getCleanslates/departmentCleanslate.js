import { GETDEPARTCLEANSLATES } from '../../actions/getCleanslates';

const initialState = {
  departGroup: null,
  isFetching: false
}

const departmentCleanslate = (state = initialState, action) => {
    switch (action.type) {
     case GETDEPARTCLEANSLATES:
      return {
        departGroup: action.departArrary,
        isFetching: true,
      };
      default:
        return state;
    }
  };
  
  export default departmentCleanslate