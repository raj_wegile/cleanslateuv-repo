import { GETDEVICE } from '../actions/hospiManager/getDevices';

const initialState = {
  device:null,
  isFetching: false
}

const managerDevice = (state = initialState, action) => {
    switch (action.type) {
      case GETDEVICE:
          return {
            device: action.device,
            isFetching: true,
          };
      default:
        return state;
    }
  };
  
  export default managerDevice