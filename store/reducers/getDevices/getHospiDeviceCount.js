import { GETHOSPITALDEVICESUCCEEDED } from '../../actions/getDevices';

const initialState = {
  group:null,
  departNameArray: null,
  hospiDeviceArray: null,
  isFetching: false
}

const getHospiDeviceCount = (state = initialState, action) => {
    switch (action.type) {
      case GETHOSPITALDEVICESUCCEEDED:
        return {
            group:action.group,
            departNameArray: action.departNameArray,
            hospiDeviceArray: action.hospiDeviceArray,
            isFetching: true
        };
      default:
        return state;
    }
  };
  
  export default getHospiDeviceCount