import { GETDEVICEOFDEPARMENT } from '../../actions/getDevices';

const initialState = {
  device:null,
  isFetching: false
}

const getDeviceOfDepart = (state = initialState, action) => {
    switch (action.type) {
      case GETDEVICEOFDEPARMENT:
          return {
            device: action.device,
            isFetching: true,
          };
      default:
        return state;
    }
  };
  
  export default getDeviceOfDepart