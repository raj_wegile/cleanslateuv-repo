import { GETDEPARTDEVICES } from '../../actions/getDevices';
import { GETMANAGERDEPARTDEVICES } from '../../actions/getDevices'

const initialState = {
  departDevicesArray:[],
  isFetching: false
}

const departmentDevice = (state = initialState, action) => {
    switch (action.type) {
      case GETDEPARTDEVICES:
          return {
            departDevicesArray: action.departDevicesArray,
            isFetching: true,
          };
      case GETMANAGERDEPARTDEVICES:
        return {
          departDevicesArray: action.deviceTypeGroups,
          isFetching: true,
        };
      default:
        return state;
    }
  };
  
  export default departmentDevice