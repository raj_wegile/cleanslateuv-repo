import { GETDEVICEARRAY } from '../../actions/getDevices';

const initialState = {
  devices:null,
  isFetching: false
}

const getDeviceArray = (state = initialState, action) => {
    switch (action.type) {
      case GETDEVICEARRAY:
          return {
            devices: action.devices,
            isFetching: true,
          };
      default:
        return state;
    }
  };
  
  export default getDeviceArray