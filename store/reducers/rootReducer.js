import { combineReducers } from 'redux'
import auth from './auth/auth'
import getDevice from './userDevice/device'
import getDeviceCount from './userDevice/deviceCount'
import userLogin from './getUsers/userLogin'
import activeUsers from './getUsers/user'
import formReducer from './auth/form'
import changePassword from './auth/changePassword'
import forgotPassword from './auth/forgotPassword'
import getHospiCleanslateCount from './getCleanslates/getHospiCleanslateCount'
import departmentCleanslate from './getCleanslates/departmentCleanslate'
import cleanslate from './getCleanslates/cleanslate'
import getHospiDeviceCount from './getDevices/getHospiDeviceCount'
import departmentDevice from './getDevices/departmentDevice'
import getDeviceArray from './getDevices/getDevices'
import getDeviceOfDepart from './getDevices/deviceOfDepart'
import departHospiCleanslate from './getCleanslates/departHospiCleanslate'

const rootReducer = combineReducers({
  cleanslate:cleanslate,
  departmentCleanslate: departmentCleanslate,
  departHospiCleanslate: departHospiCleanslate,
  departmentDevice:departmentDevice,
  getDeviceArray:getDeviceArray,
  getDeviceOfDepart:getDeviceOfDepart,
  auth: auth,
  getDevice: getDevice,
  getDeviceCount:getDeviceCount,
  userLogin:userLogin,
  activeUsers:activeUsers,
  formReducer:formReducer,
  changePassword:changePassword,
  forgotPassword:forgotPassword,
  getHospiCleanslateCount:getHospiCleanslateCount,
  getHospiDeviceCount: getHospiDeviceCount,
})

export default rootReducer