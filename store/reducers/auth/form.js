const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const initialState = {
  inputValues: {
    username: '',
  },
  inputValidities: {
    username: false,
  },
  formIsValid: false
}

const formReducer = (state = initialState, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value
    };
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid
    };
    let updatedFormIsValid = true;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues
    };
  }
  return state;
};

export default formReducer