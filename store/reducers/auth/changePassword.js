import { SIGNIN, CHANGEPASSWORDSUCCEEDED } from '../../actions/auth';

const initialState = {
  // token: null,
  // userId: null,
  user: null,
  isFetching: true
};

const changePassword = (state = initialState, action) => {
  switch (action.type) {
    case SIGNIN:
      return {
        isFetching: true
      };
    case CHANGEPASSWORDSUCCEEDED:
      return {
        user: action.user,
        isFetching: false
      };
    default:
      return state;
  }
};

export default changePassword