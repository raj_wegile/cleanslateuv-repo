import { GETCODESUCCEEDED, PASSWORDSUBMITTED } from '../../actions/auth';

const initialState = {
  code: null,
  isSubmitted: false
};

const forgotPassword = (state = initialState, action) => {
  switch (action.type) {
    case GETCODESUCCEEDED:
      return {
        code: action.code
      };
    case PASSWORDSUBMITTED:
      return {
        isSubmitted: true
      };
    default:
      return state;
  }
};

export default forgotPassword