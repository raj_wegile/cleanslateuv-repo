import { SIGNIN, SIGNINSUCCEEDED, SIGNOUTSUCCEEDED, LOGOUTTOKEN } from '../../actions/auth';

const initialState = {
  user: null,
  isFetching: false,
  status: null
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case SIGNIN:
      return {
        isFetching: true
      };
    case SIGNINSUCCEEDED:
      return {
        user: action.user,
        isFetching: false
      };
     case SIGNOUTSUCCEEDED:
       return {
        user: action.user,
        isFetching: false
       };
     case LOGOUTTOKEN:
      return {
        user: null,
        isFetching: false,
        status:action.status
       };
    default:
      return state;
  }
};

export default auth