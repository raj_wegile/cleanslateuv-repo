import { GETDEVICESUCCEEDED } from '../../actions/userDevice';

const initialState = {
  user:null,
  device_id: null,
  deviceName: null,
  Tag_Number:null,
  usage:null,
  unit:null,
  DepartUsername:null,
  status:false,
  isFetching: false
};

const getDevice = (state = initialState, action) => {
  switch (action.type) {
    case GETDEVICESUCCEEDED:
      return {
        user:action.user,
        device_id:action.user.device_id,
        deviceName:action.user.deviceName,
        Tag_Number:action.user.Tag_Number,
        usage:action.user.usage,
        unit:action.user.unit,
        DepartUsername:action.user.HositalUsername,
        isFetching: true
      };
    // case SIGNUP:
    //   return {
    //     token: action.token,
    //     userId: action.userId
    //   };
    default:
      return state;
  }
};

export default getDevice