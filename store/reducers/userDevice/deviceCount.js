import { GETDCOUNTSUCCEEDED } from '../../actions/userDevice';

const initialState = {
  countToday:null,
  countP7:null,
  countP30:null,
  countAll:null,
  isFetching: false,
};

const getDeviceCount = (state = initialState, action) => {
  switch (action.type) {
    case GETDCOUNTSUCCEEDED:
      return {
         countToday:action.count[0],
         countP7:action.count[1],
         countP30:action.count[2],
         countAll:action.count[3],
         isFetching: true,
      };
    default:
      return state;
  }
};

export default getDeviceCount