import {GETUSERSSUCCEEDED} from '../../actions/getUsers';

const initialState = {
  userArray: null,
  isFetching: false
}

const activeUsers = (state = initialState, action) => {
    switch (action.type) {
     case GETUSERSSUCCEEDED:
          return {
            userArray: action.userArray,
            isFetching: true
          }
      default:
        return state;
    }
  };
  
export default activeUsers