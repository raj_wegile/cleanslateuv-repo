import {GETUSERLOGINSUCCEEDED} from '../../actions/getUsers';

const initialState = {
  confirmation: null,
  isFetching: false
}

const userLogin = (state = initialState, action) => {
    switch (action.type) {
     case GETUSERLOGINSUCCEEDED:
          return {
            confirmation: action.confirmation,
            isFetching: true
          }
      default:
        return state;
    }
  };
  
export default userLogin