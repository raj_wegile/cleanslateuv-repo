
import { Auth } from 'aws-amplify';
export const LOGOUT = 'LOGOUT';
export const LOGOUTSUCCEEDED = 'LOGOUTSUCCEEDED';
export const SIGNIN = 'SIGNIN';
export const UPDATEMANAGERTOKEN = 'UPDATEMANAGERTOKEN';
export const UPDATEDEVICETOKEN = 'UPDATEDEVICETOKEN';
export const SIGNOUT = 'SIGNOUT';
export const LOGOUTTOKEN = 'LOGOUTTOKEN'


export const SIGNINSUCCEEDED = 'SIGNINSUCCEEDED';
function signInSuceeded(user) {
    return {
        type: SIGNINSUCCEEDED,
        user
    }
}

export const signIn = (username, password) => {
    return async dispatch => {
        const user = await Auth.signIn(username, password);
        return dispatch(signInSuceeded(user));
    }
}

export const SIGNOUTSUCCEEDED = 'SIGNOUTSUCCEEDED';
function signOutSucceeded(user) {
    return {
        type: SIGNOUTSUCCEEDED,
        user
    }
}

export const signOut = ( ) => {
    return async dispatch => {
         const user = await Auth.signOut()
         return dispatch(signOutSucceeded(user));
    }
}

export const CHANGEPASSWORDSUCCEEDED = 'CHANGEPASSWORDSUCCEEDED';
function changePasswordSuceeded(user) {
    return {
        type: CHANGEPASSWORDSUCCEEDED,
        user
    }
}

export const changePassword = (_user, password) => {
    return async dispatch => {
        const user = await Auth.completeNewPassword(_user, password);
        return dispatch(changePasswordSuceeded(user));
    }
}

export const GETCODESUCCEEDED = 'GETCODESUCCEEDED';
function getCodeSuceeded(code) {
    return {
        type: GETCODESUCCEEDED,
        code
    }
}

export const getCode = (username) => {
    return async dispatch => {
        const code = await Auth.forgotPassword(username);
        return dispatch(getCodeSuceeded(code));
    }
}

export const PASSWORDSUBMITTED = 'PASSWORDSUBMITTED';
function passwordSubmitted(data) {
    return {
        type: CHANGEPASSWORDSUCCEEDED,
        data
    }
}

export const forgotPasswordSubmit = (username, code, new_password) => {
    return async dispatch => {
        const data = await Auth.forgotPasswordSubmit(username, code, new_password);
        return dispatch(passwordSubmitted(data));
    }
}

export const updateDeviceToken = (user, token) => {
    console.log(user, token)
    return async dispatch => {
        const status = await fetch('https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/devicenotification', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              operand1: user,
              operand2: token,
            }),
          });
         dispatch( {
             type: UPDATEDEVICETOKEN,
             status:status
          })
    }
}