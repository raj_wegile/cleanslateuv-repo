export const GETDEPARTDEVIES = 'GETDEPARTDEVIES';
export const GETDEVICEARRAY = 'GETDEVICEARRAY'
export const GETHOSPITALDEVICESUCCEEDED = 'GETHOSPITALDEVICESUCCEEDED';
export const GETDEPARTDEVICES = 'GETDEPARTDEVICES';
export const GETDEVICEOFDEPARMENT = 'GETDEVICEOFDEPARMENT';
export const GETMANAGERDEPARTDEVICES = 'GETMANAGERDEPARTDEVICES'

function getHospiDeviceCountSuceeded (group){
        return  {
            type: GETHOSPITALDEVICESUCCEEDED,
            group,
            departNameArray: Object.keys(group),
            hospiDeviceArray: Object.values(group),
        }
}

export const getHospiDeviceCount = (email, date) => {
    return async dispatch => {
        const group = await fetch("https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/devicehospi?operand2=" + email+ "&operand1=" + date).then(res => res.json());
        let deviceTypeGroups = [];
        for (let key in group.body) {
             deviceTypeGroups[key] = group.body[key].reduce((r, a) => {
                r[a.deviceType] = [...r[a.deviceType] || [], a];
                return r;
               }, {});
          }
        return dispatch(getHospiDeviceCountSuceeded(deviceTypeGroups));
    }
}

export const getDepartDeviceCount = (email, date) => {
    return async dispatch => {
        const group = await fetch("https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/devicedepart?operand2=" + email+ "&operand1=" + date).then(res => res.json());
        let groupArrary =  Object.values(group.body);
        let deviceTypeGroups = [];
        deviceTypeGroups = groupArrary.reduce((r, a) => {
                r[a.deviceType] = [...r[a.deviceType] || [], a];
                return r;
               }, {});     
        dispatch( {
            type: GETMANAGERDEPARTDEVICES,
            deviceTypeGroups:Object.entries(deviceTypeGroups)
         })
    }
}

export const getDepartDevice = (departDevicesArray) => {
    return async dispatch => {
        dispatch( {
          type: GETDEPARTDEVICES,
          departDevicesArray
        })
    }
}

export const getDeviceArray=(devices) => {
    return async dispatch => {
        dispatch( {
          type: GETDEVICEARRAY,
          devices
        })
    }
}

export const getDeviceOfDepart=(device) => {
    return async dispatch => {
        dispatch( {
          type: GETDEVICEOFDEPARMENT,
          device
        })
    }
}