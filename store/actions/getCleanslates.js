export const GETDEPARTCLEANSLATES = 'GETDEPARTCLEANSLATES';
export const GETMANAGERDEPARTCLEANSLATES = 'GETMANAGERDEPARTCLEANSLATES';
export const GETCLEANSLATE = 'GETCLEANSLATE';

export const GETHOSPITALCLEANSLATESUCCEEDED = 'GETHOSPITALCLEANSLATESUCCEEDED';
function getHospitalCleanslateSuceeded(group) {
    let hospiName = Object.values(group);
    let departNameArray = Object.keys(group);
    let color = [];
    let status = [];
    let statusArray = [];
    for (let key in hospiName) {
        if ((hospiName[key].find(x => x.color === 'red')) != undefined) {
            status[key] = "Mechanical \n Problem",
                color[key] = "red"
        } else if ((hospiName[key].find(x => x.color === 'orange')) != undefined) {
            status[key] = "Buld Change",
                color[key] = "orange"
        } else {
            status[key] = "Normal",
                color[key] = "green"
        }
      statusArray[key] = [departNameArray[key], status[key], color[key], 
      hospiName[key][0].hospitalName, hospiName[key][0].email]
    }
    return {
        type: GETHOSPITALCLEANSLATESUCCEEDED,
        group,
        departNameArray: departNameArray,
        hospiName: Object.values(group),
        statusArray: statusArray
    }
}

export const getHospiCleanslateCount = (email, date) => {
    return async dispatch => {
        let cleanslateGroups = [];
        const group = await fetch("https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/cleanslatehospi?operand2=" + email + "&operand1=" + date).then(res => res.json());
        cleanslateGroups = group.body.reduce((r, a) => {
            r[a.departmentName] = [...r[a.departmentName] || [], a];
            return r;
        }, []);
        return dispatch(getHospitalCleanslateSuceeded(cleanslateGroups));
    }
}

export const GETDEPARTMENTCLEANSLATESUCCEEDED = 'GETDEPARTMENTCLEANSLATESUCCEEDED';
  function getDepartmentCleanslateSucceeded(group) {
    const departArray = group; 
    let status;
    let color;
    if ((departArray.find(x => x.color === 'red')) != undefined) {
        status = "Mechanical \n Problem",
        color = "red"
    } else if ((departArray.find(x => x.color === 'orange')) != undefined) {
        status = "Bulb Change",
        color = "orange"
    } else {
        status = "Normal",
        color = "green"
    }

    return {
        type: GETDEPARTMENTCLEANSLATESUCCEEDED,
        departArray: departArray,
        color:color,
        status:status
    }
};

export const getDepartCleanslateCount = (email, date) => {
    return async dispatch => {
        const group = await fetch( "https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/cleanslatedepart?operand2=" + email + "&operand1=" + date).then(res => res.json());
        let departCleanslate =  group.body;
        return dispatch(getDepartmentCleanslateSucceeded(departCleanslate));
    }
}

export const getDepartCleanslates = (departArrary) => {
    return async dispatch => {
        dispatch({
            type: GETDEPARTCLEANSLATES,
            departArrary: departArrary
        })
    }
}

export const getCleanslate = (cleanslate) => {
    return async dispatch => {
        dispatch({
            type: GETCLEANSLATE,
            cleanslate
        })
    }
}

