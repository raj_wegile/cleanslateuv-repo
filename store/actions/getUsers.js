export const getUserInformation = (email) => {
    return async dispatch => {
        const group = await fetch("https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/users?operand1=" + email).then(res => res.json());
        return dispatch(getCleanSlateSucceeded(group));
    }
}

export const GETUSERSSUCCEEDED = 'GETUSERSSUCCEEDED'
function getCleanSlateSucceeded(group) {
    const userArray = group;
    return {
        type: GETUSERSSUCCEEDED,
        userArray: userArray
    }
}

export const sendUserLogin = (email) => {
    return async dispatch => {
        const group = await fetch("https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/userslogin?operand1=" + email).then(res => res.json());
        return dispatch(sendUserLoginSucceeded(group));
    }
}

export const GETUSERLOGINSUCCEEDED = 'GETUSERLOGINSUCCEEDED'
function sendUserLoginSucceeded(group) {
    const confirmation = group;
    return {
        type: GETUSERSSUCCEEDED,
        confirmation: confirmation
    }
}