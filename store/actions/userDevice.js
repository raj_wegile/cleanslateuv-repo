export const UPDATETOKEN = 'UPDATETOKEN'

export const getDevice = (username) => {
    return async dispatch => {
        const user = await fetch("https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/devices/" + username).then(res => res.json());
        return dispatch(getDeviceSucceed(user.Items[0]));
    }
}

export const GETDEVICESUCCEEDED = 'GETDEVICESUCCEEDED';
function getDeviceSucceed(user) {
    return {
        type: GETDEVICESUCCEEDED,
        user
    }
}

export const getDeviceCount = (date, device_id) => {
    return async dispatch => {
        const count = await fetch("https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/counts?operand2=" + device_id + "&operand1=" + date).then(res => res.json());
        return dispatch(getDeviceCountSucceed((count.body.replace(/\]|\[/g, '')).split(',')));
    }
}

export const GETDCOUNTSUCCEEDED = 'GETDCOUNTSUCCEEDED';
function getDeviceCountSucceed(count) {
    return {
        type: GETDCOUNTSUCCEEDED,
        count
    }
}

export const updateToken = (user, token) => {
    return async dispatch => {
        const status = await fetch('https://3qzsponqzl.execute-api.us-east-2.amazonaws.com/prod/notification', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              operand1: user,
              operand2: token,
            }),
          });
         dispatch( {
             type: UPDATETOKEN,
             status:status
          })
    }
}

