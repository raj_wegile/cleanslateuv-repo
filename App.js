import React, { Component } from 'react';
import AppNavigator from './navigation/AppNavigator.js';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-community/async-storage';
import { View, Text, Image, StyleSheet, Dimensions, Platform } from 'react-native';

// Configuration for the application authentication
import Amplify from 'aws-amplify';
// If cognito user pool needs to be changed, alter this file
import awsmobile from './aws-exports';
Amplify.configure(awsmobile);

import { composeWithDevTools } from 'redux-devtools-extension'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './store/reducers/rootReducer';
import { Provider } from 'react-redux';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';

const storeData = async (value) => {
  try {
    await AsyncStorage.setItem('showApp', value)
  } catch (e) {
    // saving error
  }
}

const getData = async () => {
  const value = await AsyncStorage.getItem('showApp')

  if(value !== null)
    return value
  
  return false
}

// Accessing the store and associated components
// If adding additional dispatching action files, ensure they are added to the rootReducer
 const store = createStore(
   rootReducer,
   composeWithDevTools(applyMiddleware(thunk))
 );

 // Fonts that need to be used in the application
const fetchFonts = () => {
  return Font.loadAsync({
    'euclid-triangle': require('./assets/fonts/EuclidTriangle-Regular.ttf'),
    'euclid-triangle-bold': require('./assets/fonts/EuclidTriangle-Bold.ttf'),
    'euclid-triangle-semibold': require('./assets/fonts/EuclidTriangle-SemiBold.ttf'),
  });
};

// The intro slider for instructions on how to use the app
const slides = [
  {
    title: 'Logging In',
    text: "Ensure you have a username and password\nIf you don't have one, contact us...\n\nPhone: +1 (877) 553-6778 Ext 2\nEmail: info@cleanslateuv.com",
    image: require('./assets/login.jpg'),
    bg: '#00ADEE',
  },
  {
    title: 'Landing Screen',
    text: "Find a summary of CleanSlate's in your department\nTap one to see detailed info, and a neat graph!",
    image: Platform.OS === 'ios' ? require('./assets/landingiOS.jpg') : require('./assets/landingAndroid.jpg'),
    bg: '#003459',
  },
  {
    title: 'Connecting to a CleanSlate',
    text: "Click the Settings icon from the 'Landing Screen'\n\nThese features are still under development\nIn the meantime, send us some feature requests!",
    image: require('./assets/settings.jpg'),
    bg: '#5DC3B5',
  },
];

// Styles that are used for the intro slider
const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue'
  },
  image: {
    width: 0.85*Dimensions.get('window').width,
    height: 0.525*Dimensions.get('window').height,
    marginVertical: 28
  },
  text: {
    fontSize: 14,
    color: 'rgba(255, 255, 255, 0.8)',
    textAlign: 'center',
    fontFamily: 'euclid-triangle'
  },
  title: {
    fontSize: 32,
    color: 'white',
    textAlign: 'center',
    fontFamily: 'euclid-triangle-bold'
  },
});

export default class App extends Component {
  state = {
    fondLoaded: false,
    showMainApp: false,
  }
  constructor(props) {
    super(props)
  }

  async componentDidMount () {
    let temp = 0;
    await getData().then(function(result) { temp = result });
    this.setState({ showMainApp: temp });
  }

  onDone = () => {
    // Show real app through navigation or simply by controlling state
    this.setState({ showMainApp: true });
    storeData('true');
  }

  renderItem = ({item}) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.bg,
          },
        ]}>
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} style={styles.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };

  keyExtractor = (item) => item.title.toString();
  
  render() {
    // Load the font if it has not been done already
    if (!this.state.fontLoaded) {
      return (
        <AppLoading startAsync={fetchFonts}
          onFinish={() => {this.setState({fontLoaded: true})
          }}/>
      )
    }

    // If the intro slider has been dismissed the main app can be shown
    if (this.state.showMainApp) {
      return (
        <Provider store={store}>
          <AppNavigator/>
        </Provider>
      )
    } else {
      return (
        <AppIntroSlider data={slides} onDone={this.onDone}
        renderItem={this.renderItem} keyExtractor={this.keyExtractor}
        />
      )
    }
  }
}
